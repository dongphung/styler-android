# styler-android

# Tools:
- Android studio: https://developer.android.com/studio/index.html
- Fabric plugin for android studio: https://www.fabric.io/downloads/android

# External libs:
- FBSDK				-	https://developers.facebook.com/docs/android/getting-started
					-	https://developers.facebook.com/docs/facebook-login/android
						
- Fabric			-	https://get.fabric.io/android?locale=en-gb                        
- Crashlytics		-	https://fabric.io/kits/android/crashlytics
- Repro				-	http://docs.repro.io/ja/dev/sdk/getstarted/android.html
- GoogleAnalytics	-	https://developers.google.com/analytics/devguides/collection/android/v4/
- Realm				-	https://realm.io/news/realm-for-android/
- Adjust			- 	https://www.adjust.com/mobile-app-tracking-sdk/

- RxAndroid			-	http://reactivex.io/
                    -   https://github.com/ReactiveX/RxAndroid
                    
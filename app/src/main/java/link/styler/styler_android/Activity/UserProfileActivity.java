package link.styler.styler_android.Activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import link.styler.styler_android.Fragment.FollowingFragment;
import link.styler.styler_android.Fragment.GoingFragment;
import link.styler.styler_android.Fragment.LikesFragment;
import link.styler.styler_android.Fragment.PostFragment;
import link.styler.styler_android.R;
import link.styler.styler_android.Adapter.ViewPagerAdapter;
import link.styler.styler_android.Fragment.WatchesFragment;

@SuppressWarnings("deprecation")
public class UserProfileActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{


    private ViewPager viewPager;
    private Button btnSetting;
    private ImageView imaViewAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        //getSupportActionBar().hide();


        viewPager = (ViewPager) findViewById(R.id.viewpager_userprofile);
        addTabs(viewPager);



        btnSetting = (Button) findViewById(R.id.btn_setting);
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),SettingActivity.class);
                startActivity(i);
            }
        });

        imaViewAvatar = (ImageView)findViewById(R.id.imaViewAvatarUserProfile);
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(),R.drawable.com_facebook_profile_picture_blank_square);
        RoundedBitmapDrawable rounddeBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(),imageBitmap);

        rounddeBitmapDrawable.setCornerRadius(30.0f);
        rounddeBitmapDrawable.setAntiAlias(true);
        imaViewAvatar.setImageDrawable(rounddeBitmapDrawable);

    }

    private void addTabs (final ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PostFragment(),"Post ");
        adapter.addFrag(new LikesFragment(),"Like");
        adapter.addFrag(new WatchesFragment(),"Watched ");
        adapter.addFrag(new FollowingFragment(),"Following");
        adapter.addFrag(new GoingFragment(),"Going");

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {



                int number = viewPager.getAdapter().getCount();

                int currentPage = viewPager.getCurrentItem();

                if(number - currentPage == 2 && state == 0 ) {

                    ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();

                    adapter.addFrag(new PostFragment(), "Post ");
                    adapter.addFrag(new LikesFragment(), "Like");
                    adapter.addFrag(new WatchesFragment(), "Watched ");
                    adapter.addFrag(new FollowingFragment(), "Following");
                    adapter.addFrag(new GoingFragment(), "Going");
                    viewPager.setAdapter(adapter);
                    viewPager.setCurrentItem(currentPage, false);


                }
                else if((number - currentPage >= (number - 2)) && state == 0)
                {

                    ViewPagerAdapter adapter = (ViewPagerAdapter)viewPager.getAdapter();
                    adapter.addFrag(new PostFragment(), "Post ");
                    adapter.addFrag(new LikesFragment(), "Like");
                    adapter.addFrag(new WatchesFragment(), "Watched ");
                    adapter.addFrag(new FollowingFragment(), "Following");
                    adapter.addFrag(new GoingFragment(), "Going");
                    viewPager.setAdapter(adapter);
                    viewPager.setCurrentItem(number + currentPage,false);
                }

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        return true;
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


}
package link.styler.styler_android.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import link.styler.styler_android.Adapter.MyAdapter;
import link.styler.styler_android.ObjectClass.PersonalProfile;
import link.styler.styler_android.R;

public class TabActivity_1 extends Activity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_1);



        //GetListView object from xml
        listView = (ListView)findViewById(R.id.list);


        //Define Array values to show in ListView

        //Create object

        PersonalProfile personalProfile;

        ArrayList<PersonalProfile> personalProfiles = new ArrayList<PersonalProfile>();

        personalProfile = new PersonalProfile("Title 1","Abc","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title 2","Def","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title 3","Ghm","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title 4","Ghm","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title ","Ghm","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title ","Ghm","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);

        personalProfile = new PersonalProfile("Title 7","Ghm","abcdef sdafdsgsg",123,"asdasd","sadasd");
        personalProfiles.add(personalProfile);


        if(personalProfiles == null)
            Log.i("Quang","Array personal null");

        listView.setAdapter(new MyAdapter(this,personalProfiles));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //ListView Clicked item index
                int itemPosition = position;

            }
        });

    }
}

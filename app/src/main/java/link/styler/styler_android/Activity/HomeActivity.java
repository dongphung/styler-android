package link.styler.styler_android.Activity;


import android.animation.ObjectAnimator;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;

import link.styler.styler_android.Fragment.FollowingFragment;
import link.styler.styler_android.Fragment.GoingFragment;
import link.styler.styler_android.Fragment.LikesFragment;
import link.styler.styler_android.Fragment.PostFragment;
import link.styler.styler_android.R;
import link.styler.styler_android.Adapter.ViewPagerAdapter;
import link.styler.styler_android.Fragment.WatchesFragment;


public class HomeActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ProgressBar progressBar;
    private ObjectAnimator animation;


    private int previousState, currentState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_layout);

        //getSupportActionBar().hide();

        viewPager = (ViewPager)findViewById(R.id.viewpagerHome);
        addTab(viewPager);

        progressBar = (ProgressBar)findViewById(R.id.progressBarHome);



    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("Quang","onResume");

        findViewById(viewPager.getId()).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.i("Quang","Home AC load done");

                progressBar.setVisibility(View.INVISIBLE);



            }
        });
    }

    @Override
    protected void onPause()
    {
        Log.i("Quang","onPause");
        super.onPause();

    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("Quang","onStop");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("Quang","onDestroy");
    }

    private void addTab(final ViewPager viewPager)
    {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFrag(new PostFragment(),"Post ");
        adapter.addFrag(new LikesFragment(),"Like");
        adapter.addFrag(new WatchesFragment(),"Watched ");
        adapter.addFrag(new FollowingFragment(),"Following");
        adapter.addFrag(new GoingFragment(),"Going");



        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(2);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
           @Override
           public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

               progressBar.setVisibility(View.VISIBLE);
               animation = ObjectAnimator.ofInt(progressBar,"",500);
               animation.setDuration(5000);
               animation.setInterpolator(new DecelerateInterpolator());
               animation.start();

           }

           @Override
           public void onPageSelected(int position) {

           }

           @Override
           public void onPageScrollStateChanged(int state) {



               int number = viewPager.getAdapter().getCount();

               int currentPage = viewPager.getCurrentItem();

               if(number - currentPage == 2 && state == 0 ) {

                   ViewPagerAdapter adapter = (ViewPagerAdapter) viewPager.getAdapter();

                   adapter.addFrag(new PostFragment(), "Post ");
                   adapter.addFrag(new LikesFragment(), "Like");
                   adapter.addFrag(new WatchesFragment(), "Watched ");
                   adapter.addFrag(new FollowingFragment(), "Following");
                   adapter.addFrag(new GoingFragment(), "Going");
                   viewPager.setAdapter(adapter);
                   viewPager.setCurrentItem(currentPage, false);


               }
               else if((number - currentPage >= (number - 2)) && state == 0)
               {

                   ViewPagerAdapter adapter = (ViewPagerAdapter)viewPager.getAdapter();
                   adapter.addFrag(new PostFragment(), "Post ");
                   adapter.addFrag(new LikesFragment(), "Like");
                   adapter.addFrag(new WatchesFragment(), "Watched ");
                   adapter.addFrag(new FollowingFragment(), "Following");
                   adapter.addFrag(new GoingFragment(), "Going");
                   viewPager.setAdapter(adapter);
                   viewPager.setCurrentItem(number + currentPage,false);
               }

           }
       });
    }


}

package link.styler.styler_android.Activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import link.styler.styler_android.R;

@SuppressWarnings("deprecation")
public class CustomerActivity extends TabActivity {

    private TabHost tabhost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_layout);

        tabhost = (TabHost)getTabHost();

        TabHost.TabSpec tabMenu1 = tabhost.newTabSpec("Menu 1");
        TabHost.TabSpec tabMenu2 = tabhost.newTabSpec("Menu 2");

        tabMenu1.setIndicator("アクティビティ");
        tabMenu1.setContent(new Intent(this,TabActivity_1.class));

        tabMenu2.setIndicator("メッセージ");
        tabMenu2.setContent(new Intent(this,TabActivity_2.class));

        tabhost.addTab(tabMenu1);
        tabhost.addTab(tabMenu2);
    }
}

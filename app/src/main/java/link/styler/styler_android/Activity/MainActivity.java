package link.styler.styler_android.Activity;

import android.animation.ObjectAnimator;
import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;


//Quang Dinh import for view
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.Articles;
import link.styler.styler_android.create_post.CreatePost;

import android.util.Log;


@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {

    private TabHost tabHost;
    private static ProgressBar progressBar;
    private ObjectAnimator animation;


    private TabSpec tabSpecHome, tabSpecArtice, tabSpecPost, tabSpecCustomer, tabSpecProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //StylerApi.setCurrentActivity(this);

        //Fabric.with(this, new Crashlytics());

        // Setup Repro
        // temporarily hardcoded: replace by styler token and define it in res or using constant
        //Repro.setup(this.getApplication(), "1000541d-cbbb-4990-ac32-7e57c8a0045a");



        setContentView(R.layout.activity_main);
        findViewById(getTabHost().getId()).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Log.i("Quang","load done");

                progressBar.setVisibility(View.INVISIBLE);



            }
        });


        progressBar = (ProgressBar)findViewById(R.id.progressMainAc);




        tabHost = (TabHost)getTabHost();
        tabHost.setup();

        tabSpecHome = tabHost.newTabSpec("Home");
        tabSpecArtice = tabHost.newTabSpec("Artice");
        tabSpecPost = tabHost.newTabSpec("Post");
        tabSpecCustomer = tabHost.newTabSpec("Customer");
        tabSpecProfile = tabHost.newTabSpec("Profile");




    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.i("Quang","Main onResume");

        tabSpecHome.setContent(new Intent(this,HomeActivity.class));
        tabSpecHome.setIndicator("",getResources().getDrawable(R.drawable.icon_homefeed));
        tabHost.addTab(tabSpecHome);

        // Add 2 activity for Tan
        tabSpecArtice.setContent(new Intent(this, Articles.class));
        tabSpecArtice.setIndicator("",getResources().getDrawable(R.drawable.icon_article));
        tabHost.addTab(tabSpecArtice);

        tabSpecPost.setContent(new Intent(this, CreatePost.class));
        tabSpecPost.setIndicator("",getResources().getDrawable(R.drawable.icon_create));
        tabHost.addTab(tabSpecPost);
        //end add
        tabSpecCustomer.setContent(new Intent(this,CustomerActivity.class));
        tabSpecCustomer.setIndicator("",getResources().getDrawable(R.drawable.icon_notification));
        tabHost.addTab(tabSpecCustomer);

        tabSpecProfile.setContent(new Intent(this,UserProfileActivity.class));
        tabSpecProfile.setIndicator("",getResources().getDrawable(R.drawable.icon_user));
        tabHost.addTab(tabSpecProfile);



        //progressBar.setVisibility(View.GONE);
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                Log.i("Quang","tab change "+tabId);
                progressBar.setVisibility(View.VISIBLE);

            }
        });
    }



    @Override
    protected void onPause()
    {
        super.onPause();
        Log.i("Quang","Main onPause");
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        Log.i("Quang","Main onStop");
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        Log.i("Quang","Main onDestroy");
    }




}

package link.styler.styler_android.Activity;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import android.util.Log;

import link.styler.styler_android.Adapter.MyAdapterSetting;
import link.styler.styler_android.ObjectClass.ObjectSettings;
import link.styler.styler_android.R;

public class SettingActivity extends Activity {

    private ListView listView1;
    private ListView listView2;
    private ListView listView3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_layout);

        listView1 = (ListView)findViewById(R.id.listViewSetting1);

        ArrayList<ObjectSettings> arrayObjSettings = new ArrayList<ObjectSettings>();

        ObjectSettings objectSetting = new ObjectSettings("プッシュ通知","",false);
        arrayObjSettings.add(objectSetting);
        objectSetting = new ObjectSettings("ログイン情報","",false);
        arrayObjSettings.add(objectSetting);
        listView1.setAdapter(new MyAdapterSetting(this,arrayObjSettings));


        listView2 = (ListView)findViewById(R.id.listViewSetting2);
        ArrayList<ObjectSettings> arrayObjSettings2 = new ArrayList<ObjectSettings>();

        objectSetting = new ObjectSettings("プッシュ通知","",true);
        arrayObjSettings2.add(objectSetting);
        objectSetting = new ObjectSettings("メール通知","",true);
        arrayObjSettings2.add(objectSetting);
        objectSetting = new ObjectSettings("メールマガジン配信","",true);
        arrayObjSettings2.add(objectSetting);

        listView2.setAdapter(new MyAdapterSetting(this,arrayObjSettings2));

        listView3 = (ListView)findViewById(R.id.listViewSetting3);
        final ArrayList<ObjectSettings> arrayObjSetting3 = new ArrayList<ObjectSettings>();
        objectSetting = new ObjectSettings("利用規約","abc",false);
        arrayObjSetting3.add(objectSetting);
        objectSetting = new ObjectSettings("プライバシーポリシー","class",false);
        arrayObjSetting3.add(objectSetting);
        objectSetting = new ObjectSettings("ヘルプ","def",false);
        arrayObjSetting3.add(objectSetting);
        objectSetting = new ObjectSettings("会社情報","ght",false);
        arrayObjSetting3.add(objectSetting);
        objectSetting = new ObjectSettings("ライセンス","mno",false);
        arrayObjSetting3.add(objectSetting);

        listView3.setAdapter(new MyAdapterSetting(this,arrayObjSetting3));

        listView3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("Quang","list view 3 click " +position+" id "+id);
                Log.i("Quang","arrayObjSetting3 " + arrayObjSetting3.get(position).GetNextActivity());
                //String nextActivity = arrayObjSetting3.get(position).GetNextActivity();
                //Intent i = new Intent(getApplicationContext(),PravcyActivity.class);
                //startActivity(i);

            }
        });

    }
}

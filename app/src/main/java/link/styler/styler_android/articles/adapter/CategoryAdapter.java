package link.styler.styler_android.articles.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.ArticlesList;


/**
 * Created by macOS on 1/23/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    private List<String> listCategory;
    private Context context;

    public CategoryAdapter(List<String> listCategory, Context context) {
        this.listCategory = listCategory;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articles_btn_category, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String categoryName = listCategory.get(position);
        holder.btnCategory.setText(categoryName);
        holder.btnCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArticlesList.nameList = (String) holder.btnCategory.getText();
                Intent intent = new Intent(context, ArticlesList.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public Button btnCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            btnCategory = (Button) itemView.findViewById(R.id.btnCategory);

        }
    }

}
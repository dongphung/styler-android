package link.styler.styler_android.articles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.adapter.KeywordAdapter;
import link.styler.styler_android.articles.adapter.KeywordListAdapter;

public class ArticlesKeywordList extends AppCompatActivity {

    ImageButton btnPrevious;

    ListView lvKeyword;
    ArrayList<String>keywordList;
    KeywordListAdapter keywordListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles_keyword_list);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lvKeyword.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArticlesList.nameList = keywordList.get(position);
                Intent intent = new Intent(getApplicationContext(), ArticlesList.class);
                startActivity(intent);
            }
        });
    }

    private void addControls() {
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        createListViewKeyword();
    }

    private void createListViewKeyword() {
        lvKeyword = (ListView) findViewById(R.id.lvKeyword);
        keywordList = new ArrayList<>();
        //// TODO: 1/25/17 add data keyword listview
        for(int i = 0; i<15;i++) {
            String s = "keyword " + i;
            keywordList.add(s);
        }

        keywordListAdapter = new KeywordListAdapter(this,
                R.layout.articles_keyword_list_item,
                keywordList);
        lvKeyword.setAdapter(keywordListAdapter);
    }

}

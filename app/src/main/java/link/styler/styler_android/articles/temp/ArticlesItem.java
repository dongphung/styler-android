package link.styler.styler_android.articles.temp;

import android.graphics.Bitmap;

/**
 * Created by macOS on 1/16/17.
 */

public class ArticlesItem {

    //// TODO: 1/19/17  Đây Là Item giả cần truyền Item thiệt!!!!!!!!!!!!

    private String CellUrl;
    private String CellInfo;
    private String CellDate;
    private Bitmap CellBitmap;

    public ArticlesItem() {
    }

    public ArticlesItem(String cellUrl, String cellInfo, String cellDate, Bitmap cellBitmap) {
        CellUrl = cellUrl;
        CellInfo = cellInfo;
        CellDate = cellDate;
        CellBitmap = cellBitmap;
    }

    public String getCellUrl() {
        return CellUrl;
    }

    public void setCellUrl(String cellUrl) {
        CellUrl = cellUrl;
    }

    public String getCellInfo() {
        return CellInfo;
    }

    public void setCellInfo(String cellInfo) {
        CellInfo = cellInfo;
    }

    public String getCellDate() {
        return CellDate;
    }

    public void setCellDate(String cellDate) {
        CellDate = cellDate;
    }

    public Bitmap getCellBitmap() {
        return CellBitmap;
    }

    public void setCellBitmap(Bitmap cellBitmap) {
        CellBitmap = cellBitmap;
    }
}

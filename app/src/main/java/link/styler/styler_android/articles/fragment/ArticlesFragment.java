package link.styler.styler_android.articles.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.temp.ArticlesItem;

/**
 * Created by macOS on 1/16/17.
 */

public class ArticlesFragment extends Fragment {

    public ArticlesItem articlesItem;

    TextView txtDate;
    TextView txtTitle;
    ImageView imgFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View  view = inflater.inflate(R.layout.articles_fragment_pager, container, false);

        txtDate = (TextView) view.findViewById(R.id.txtDate);
        txtTitle= (TextView) view.findViewById(R.id.txtTitle);
        if(imgFragment!=null)
            imgFragment.setImageBitmap(this.articlesItem.getCellBitmap());
        txtDate.setText(this.articlesItem.getCellDate());
        txtTitle.setText(this.articlesItem.getCellInfo());
        return view;
    }

    public ArticlesFragment() {
    }
    public ArticlesFragment(ArticlesItem articlesItem) {
        this.articlesItem = articlesItem;
    }
}
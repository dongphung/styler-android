package link.styler.styler_android.articles.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import link.styler.styler_android.articles.fragment.ArticlesFragment;


/**
 * Created by macOS on 1/16/17.
 */

//BaseApter

public class PagerAdapter extends FragmentStatePagerAdapter {

    private int fragmentCout = 1;
    private ArrayList<ArticlesFragment>fragmentList ;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }
    @Override
    public int getCount() {
        return fragmentCout;
    }

    public PagerAdapter(FragmentManager fm, ArrayList<ArticlesFragment> fragmentList, int fragmentCout) {
        super(fm);
        this.fragmentList = fragmentList;
        this.fragmentCout = fragmentCout;
    }

    public int getFragmentCout() {
        return fragmentCout;
    }

    public void setFragmentCout(int fragmentCout) {
        this.fragmentCout = fragmentCout;
    }

    public ArrayList<ArticlesFragment>getFragmentList() {
        return fragmentList;
    }

    public void setFragmentList(ArrayList<ArticlesFragment> fragmentList) {
        this.fragmentList = new ArrayList<ArticlesFragment>();
        if (fragmentList != null)
            this.fragmentList.addAll(fragmentList);
    }
}

package link.styler.styler_android.articles.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.ArticlesList;

/**
 * Created by macOS on 1/24/17.
 */

public class KeywordAdapter extends RecyclerView.Adapter<KeywordAdapter.ViewHolder> {

    private List<String> listKeyword;
    private Context context;

    public KeywordAdapter(List<String> listKeyword, Context context) {
        this.listKeyword = listKeyword;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.articles_btn_keyword, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        String keyword = listKeyword.get(position);
        holder.btnKeyword.setText(keyword);
        holder.btnKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArticlesList.nameList = (String) holder.btnKeyword.getText();
                Intent intent = new Intent(context, ArticlesList.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listKeyword.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        public Button btnKeyword;

        public ViewHolder(View itemView) {
            super(itemView);
            btnKeyword = (Button) itemView.findViewById(R.id.btnKeyword);

        }
    }

}
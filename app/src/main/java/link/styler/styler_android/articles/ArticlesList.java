package link.styler.styler_android.articles;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.adapter.ArticlesAdapter;
import link.styler.styler_android.articles.temp.ArticlesItem;

public class ArticlesList extends AppCompatActivity {

    //// TODO: 1/20/17  truyền data cho ArticlesList
    public static String nameList = "";
    public static ArrayList<ArticlesItem> arrayListArticles;

    TextView txtNameList;
    ImageButton btnPrevious;
    ListView lvArticles;
    ArticlesAdapter articlesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artilces_list);

        Toast.makeText(getApplicationContext(), nameList, Toast.LENGTH_LONG).show();

        addControls();
        addEvents();
    }

    private void addEvents() {
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lvArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArticlesSingle.url = arrayListArticles.get(position).getCellUrl();
                Intent intent = new Intent(getApplicationContext(), ArticlesSingle.class);
                startActivity(intent);
            }
        });
    }

    private void addControls() {
        txtNameList = (TextView) findViewById(R.id.txtNameList);
        txtNameList.setText(nameList);
        btnPrevious = (ImageButton) findViewById(R.id.btnPrevious);
        lvArticles = (ListView) findViewById(R.id.lvArticles);
        arrayListArticles = new ArrayList<>();
        addDatalvArticles();
        articlesAdapter = new ArticlesAdapter(this,
                R.layout.articles_item,
                arrayListArticles);
        lvArticles.setAdapter(articlesAdapter);
    }
    
    private void addDatalvArticles() {
        switch (nameList)
        {
            case " ":
                break;

            case "  ":
                break;

            default:
                break;
        }

        for (int i = 0; i< 10;i++) {
            ArticlesItem articlesItem = new ArticlesItem("https://styler.link", nameList +" "+ i, "1/1/2017", null);
            arrayListArticles.add(articlesItem);
        }
    }

}

package link.styler.styler_android.articles;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import link.styler.styler_android.R;

public class ArticlesSingle extends AppCompatActivity {

    ImageButton btnDone;
    TextView txtUrl;
    ImageButton btnReload;
    WebView webViewArticlesSingle;
    ImageButton btnBack;
    ImageButton btnForward;
    ImageButton btnCombined;
    public static String url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles_single);
        addControls();
        addEvents();
    }

    private void addEvents() {
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webViewArticlesSingle.reload();
                txtUrl.setText(webViewArticlesSingle.getUrl());
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(webViewArticlesSingle.canGoBack()) {
                    webViewArticlesSingle.goBack();
                    txtUrl.setText(webViewArticlesSingle.getUrl());
                }
            }
        });
        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(webViewArticlesSingle.canGoForward()){
                    webViewArticlesSingle.goForward();
                    txtUrl.setText(webViewArticlesSingle.getUrl());
                }
            }
        });
        btnCombined.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openShareMode();
            }
        });
    }


    private void addControls() {
        btnDone = (ImageButton) findViewById(R.id.btnDone);
        txtUrl = (TextView) findViewById(R.id.txtUrl);
        btnReload = (ImageButton) findViewById(R.id.btnReload);

        webViewArticlesSingle = (WebView) findViewById(R.id.webViewArticlesSingle);
        webViewArticlesSingle.setWebViewClient(new WebViewClient());
        addUrl();
        webViewArticlesSingle.loadUrl(txtUrl.getText().toString());

        btnBack = (ImageButton) findViewById(R.id.btnBack);
        btnForward = (ImageButton) findViewById(R.id.btnForward);
        btnCombined = (ImageButton) findViewById(R.id.btnCombined);
    }

    private void addUrl() {
        if (url == "")
            url = "http://google.com";  // test webView
        txtUrl.setText(url);
    }
    private void openShareMode() {

    }

}

package link.styler.styler_android.articles.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import link.styler.styler_android.R;
import link.styler.styler_android.articles.temp.ArticlesItem;

/**
 * Created by macOS on 1/16/17.
 */

public class ArticlesAdapter extends ArrayAdapter<ArticlesItem> {
    Activity context;
    int resource;
    List<ArticlesItem> objects;

    public ArticlesAdapter(Activity context, int resource, List<ArticlesItem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.objects = objects;
        this.resource = resource;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = this.context.getLayoutInflater();
        View item = inflater.inflate(this.resource,null);

        ArticlesItem articlesItem = this.objects.get(position);
        ImageView imgCell = (ImageView) item.findViewById(R.id.imgArticlesCell);
        TextView txtCellTitle = (TextView) item.findViewById(R.id.txtArticlesCellTitle);
        TextView txtCellDate = (TextView) item.findViewById(R.id.txtArticlesCellDate);

        imgCell.setImageBitmap(articlesItem.getCellBitmap());
        txtCellTitle.setText(articlesItem.getCellInfo());
        txtCellDate.setText(articlesItem.getCellDate());

        return item;
    }
}

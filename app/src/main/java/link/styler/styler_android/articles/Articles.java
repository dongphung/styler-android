package link.styler.styler_android.articles;

import android.content.Intent;
import android.graphics.Point;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

import link.styler.styler_android.R;
import link.styler.styler_android.articles.adapter.ArticlesAdapter;
import link.styler.styler_android.articles.adapter.KeywordAdapter;
import link.styler.styler_android.articles.adapter.PagerAdapter;
import link.styler.styler_android.articles.fragment.ArticlesFragment;
import link.styler.styler_android.articles.temp.ArticlesItem;

public class Articles extends AppCompatActivity {

    ViewPager vpArticlesHead;
    PagerAdapter pagerAdapter;
    ArrayList<ArticlesFragment> articlesFragmentArrayList = new ArrayList<ArticlesFragment>();
    int positionVp;

    ListView lvNewArticles;
    ArrayList<ArticlesItem>arrayListNewArticles;
    ArticlesAdapter articlesAdapter;

    Button btnOpenNewArticlesList;

    RecyclerView rvCategory;
    KeywordAdapter categoryAdapter;
    ArrayList<String>arrayListCategory;

    RecyclerView rvKeyword;
    KeywordAdapter keywordAdapter;
    ArrayList<String>arrayListKeyword;

    Button btnOpenKeywordList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);
        addControls();
        addEvents();
    }

    private void addEvents() {
        vpArticlesHead.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                positionVp = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        lvNewArticles.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArticlesSingle.url = arrayListNewArticles.get(position).getCellUrl();
                Intent intent = new Intent(getApplicationContext(), ArticlesSingle.class);
                startActivity(intent);
            }
        });
        btnOpenNewArticlesList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //// TODO: 1/24/17 articlesList = new list
                ArticlesList.nameList = "newList";
                Intent intent = new Intent(getApplicationContext(), ArticlesList.class);
                startActivity(intent);
            }
        });

        btnOpenKeywordList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ArticlesKeywordList.class);
                startActivity(intent);
            }
        });
    }
    private void addControls() {
        createViewPager();
        createListViewArticles();
        //open NewPostList
        btnOpenNewArticlesList = (Button) findViewById(R.id.btnOpenNewArticlesList);
        createListViewCategoryBtn();
        createListKeywordBtn();
        //open KeywordList
        btnOpenKeywordList = (Button) findViewById(R.id.btnViewKeywordList);
    }

    private void createViewPager() {
        vpArticlesHead = (ViewPager) findViewById(R.id.vpArticlesHead);
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        //// TODO: 1/20/17 truyền data vào ViewPager Articles qua biến articlesFragmentArrayList, có thể sử dụng articlesFragmentArrayList.addAll
        ArticlesItem articlesItem = new ArticlesItem("https://styler.link/", "aaaa ", "10/10/2017", null);  //data giả
        ArticlesFragment fr = new ArticlesFragment(articlesItem);
        articlesFragmentArrayList.add(0,fr);

        ArticlesItem articlesItem1 = new ArticlesItem("http://google.com", "bbb ", "1/1/2017", null);       //data giả
        ArticlesFragment fr1 = new ArticlesFragment(articlesItem1);
        articlesFragmentArrayList.add(1,fr1);


        pagerAdapter.setFragmentList(articlesFragmentArrayList);
        pagerAdapter.setFragmentCout(articlesFragmentArrayList.size());
        vpArticlesHead.setAdapter(pagerAdapter);
    }
    private void createListViewArticles() {
        lvNewArticles = (ListView) findViewById(R.id.lvNewArticles);
        arrayListNewArticles = new ArrayList<>();

        //// TODO: 1/20/17 truyền data vào ListView  Articles qua biến arrayListNewArticles có thể sử dụng arrayListNewArticles.addAll
        for(int i = 0; i<5;i++) {
            ArticlesItem ac = new ArticlesItem("https://styler.link/", "new articles "+ i, "10/10/2017", null);      //data giả
            arrayListNewArticles.add(ac);
        }

        articlesAdapter = new ArticlesAdapter(this,
                R.layout.articles_item,
                arrayListNewArticles);
        lvNewArticles.setAdapter(articlesAdapter);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        ViewGroup.LayoutParams params = lvNewArticles.getLayoutParams();
        params.height = (int) (0.32 * size.x * arrayListNewArticles.size());
        lvNewArticles.setLayoutParams(params);
    }
    private void createListViewCategoryBtn() {
        rvCategory = (RecyclerView) findViewById(R.id.rvCategory);
        rvCategory.setHasFixedSize(true);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvCategory.setLayoutManager(layoutManager);

        arrayListCategory = new ArrayList<>();

        //// TODO: 1/25/17 Articles category giả
        for(int i = 0; i<10;i++) {
            String s = ("category" + i + " "+ i);      //data giả
            arrayListCategory.add(s);
        }
        categoryAdapter = new KeywordAdapter(arrayListCategory,this);
        rvCategory.setAdapter(categoryAdapter);

    }
    private void createListKeywordBtn() {
        rvKeyword = (RecyclerView) findViewById(R.id.rvKeyword);
        rvKeyword.setHasFixedSize(true);
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvKeyword.setLayoutManager(layoutManager);
        //// TODO: 1/25/17 Articles keyword giả
        arrayListKeyword = new ArrayList<>();
        for(int i = 0; i<10;i++) {
            String s = ("keyword" + i + " "+ i);      //data giả
            arrayListKeyword.add(s);
        }
        keywordAdapter = new KeywordAdapter(arrayListKeyword,this);
        rvKeyword.setAdapter(keywordAdapter);

    }
    public void clickViewPage(View v) {
        articlesFragmentArrayList.get(positionVp).articlesItem.getCellUrl();
        ArticlesSingle.url= articlesFragmentArrayList.get(positionVp).articlesItem.getCellUrl();
        Intent intent = new Intent(getApplicationContext(), ArticlesSingle.class);
        startActivity(intent);
    }

}

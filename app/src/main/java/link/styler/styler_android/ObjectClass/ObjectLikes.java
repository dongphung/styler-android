package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/18/17.
 */

public class ObjectLikes {

    private String pathImgAvatar;
    private String txtTitle;
    private String txtName;
    private String txtPrice;
    private int numberike;

    public ObjectLikes (String pathImgAvatar, String txtTitle, String txtName, String txtPrice, int numberLike)
    {
        this.pathImgAvatar  = pathImgAvatar;
        this.txtTitle       = txtTitle;
        this.txtName        = txtName;
        this.txtPrice       = txtPrice;
        this.numberike      = numberLike;
    }

    public String GetPathImgAvatar()
    {
        return this.pathImgAvatar.toString();
    }

    public String GetTxtTitle()
    {
        return this.txtTitle.toString();
    }

    public String GetTxtName()
    {
        return this.txtName.toString();
    }

    public String GetTxtPrice()
    {
        return "¥" + this.txtPrice.toString();
    }

    public int GetNumberLike()
    {
        return this.numberike;
    }


}

package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/18/17.
 */

public class ObjectPosts {
    private String pathImgAvatar;
    private String txtName;
    private int age;
    private boolean isSex;
    private String txtTitle;
    private String txtPost;

    public ObjectPosts (String pathImgAvatar, String txtName, int age, boolean isSex, String txtTile, String txtPost)
    {
        this.pathImgAvatar = pathImgAvatar;
        this.txtName       = txtName;
        this.age           = age;
        this.isSex         = isSex;
        this.txtTitle      = txtTile;
        this.txtPost       = txtPost;
    }

    public String GetPathImgAvatar()
    {
        return this.pathImgAvatar.toString();
    }

    public String GetTxtName()
    {
        return this.txtName.toString();
    }

    public int GetAge()
    {
        return this.age;
    }

    public boolean GetIsSex()
    {
        return this.isSex;
    }

    public String GetTxtTile()
    {
        return this.txtTitle.toString();
    }

    public String GetTxtPost()
    {
        return this.txtPost.toString();
    }

}

package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/16/17.
 */

public class PersonalProfile {
    /*  :
        Name : string
        escription : String
        Price        : Number
        Path Avatar       :   String
        Path ImageLike    :   String
      */

    private String title;
    private String name;
    private String description;
    private int price;
    private String pathImageAvatar;
    private String pathImage;

    public PersonalProfile (String title,String name, String description, int price, String pathImage, String pathImageAvatar){
        this.title = title ;
        this.name = name;
        this.description = description;
        this.price = price;
        this.pathImage = pathImage;
        this.pathImageAvatar = pathImageAvatar;
    }

    public String PersonalProfile_GetTitle()
    {
        return this.title.toString();
    }

    public String PersonalProfile_GetName()
    {
        return this.name.toString() ;
    }

    public String PersonalProfile_GetDescription()
    {
        return this.description.toString();
    }

    public String PersonalProfile_PathImageAvatar()
    {
        return this.pathImageAvatar.toString();
    }

    public String PersonalProfile_PathImage()
    {
        return this.pathImage.toString();
    }

    public int PersonalProfile_GetPrice()
    {
        return this.price;
    }

}


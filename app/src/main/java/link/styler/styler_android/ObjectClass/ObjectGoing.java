package link.styler.styler_android.ObjectClass;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by admin on 1/19/17.
 */

public class ObjectGoing {

    private String pathImage;
    private String txtTitle;
    private int numberGoing;
    private Date date;
    private String pathImaAvatar;
    private String txtName;

    public ObjectGoing(String pathImage, String txtTitle, int numberGoing, Date date, String pathImaAvatar, String txtName)
    {
        this.pathImage = pathImage;
        this.txtTitle  = txtTitle;
        this.numberGoing    = numberGoing;
        this.date      = date;
        this.pathImaAvatar  = pathImaAvatar;
        this.txtName   = txtName;
    }

    public String GetPathImage()
    {
        return this.pathImage.toString();
    }

    public String GetTxtTitle()
    {
        return this.txtTitle.toString();
    }

    public int GetNumberGoing()
    {
        return this.numberGoing;
    }

    public String GetDate()
    {
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
        return ft.format(this.date);
    }

    public String GetPathImaAvatar()
    {
        return this.pathImaAvatar.toString();
    }

    public String GetTxtName()
    {
        return this.txtName.toString();
    }
}

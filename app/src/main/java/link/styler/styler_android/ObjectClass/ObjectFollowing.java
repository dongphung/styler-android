package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/19/17.
 */

public class ObjectFollowing {

    private String pathImaAvatar;
    private String pathImaMessege;
    private int numberReply;
    private int numberFollow;
    private int numberLike;
    private String txtName;

    public ObjectFollowing(String pathImaAvatar, String pathImaMessege, int numberReply, int numberFollow, int numberLike, String txtName)
    {
        this.pathImaAvatar = pathImaAvatar;
        this.pathImaMessege = pathImaMessege;
        this.numberReply    = numberReply;
        this.numberFollow   = numberFollow;
        this.numberLike     = numberLike;
        this.txtName        = txtName;
    }

    public String GetPathImaAvatar()
    {
        return this.pathImaAvatar.toString();
    }

    public String GetPathImaMessege()
    {
        return this.pathImaMessege.toString();
    }

    public int GetNumberReply()
    {
        return this.numberReply;
    }

    public int GetNumberFollow()
    {
        return this.numberFollow;
    }

    public int GetNumberLike()
    {
        return this.numberLike;
    }

    public String GetTxtName()
    {
        return this.txtName.toString();
    }

}

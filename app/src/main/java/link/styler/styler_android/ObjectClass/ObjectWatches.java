package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/19/17.
 */

public class ObjectWatches {
    private String pathImaAvatar;
    private String txtName;
    private String txtAge;
    private boolean isSex;
    private String txtTitle;
    private String txtContentWatch;

    public ObjectWatches(String pathImaAvatar, String txtName, String txtAge, boolean isSex, String txtTitle, String txtContentWatch)
    {
        this.pathImaAvatar = pathImaAvatar;
        this.txtName       = txtName;
        this.txtAge        = txtAge;
        this.isSex         = isSex;
        this.txtTitle      = txtTitle;
        this.txtContentWatch    = txtContentWatch;
    }

    public String GetPathImaAvatar()
    {
        return this.pathImaAvatar.toString();
    }

    public String GetTxtName()
    {
        return this.txtName.toString();
    }

    public String GetTxtAge()
    {
        return this.txtAge.toString();
    }

    public boolean GetIsSex()
    {
        return this.isSex;
    }

    public String GetTxtTile()
    {
        return this.txtTitle.toString();
    }

    public String GetTxtConentWatch()
    {
        return this.txtContentWatch.toString();
    }
}

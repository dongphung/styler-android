package link.styler.styler_android.ObjectClass;

/**
 * Created by admin on 1/17/17.
 */

public class ObjectSettings {
    private String title;
    private String nextActivity;
    private boolean switchButton;

    public ObjectSettings(String title, String nextActivity, boolean switchButton)
    {
        this.title = title;
        this.nextActivity = nextActivity;
        this.switchButton = switchButton;
    }

    public String GetTitle()
    {
        return this.title.toString();
    }

    public String GetNextActivity()
    {
        return this.nextActivity.toString();
    }

    public boolean IsSwitchButton()
    {
        return this.switchButton;
    }
}

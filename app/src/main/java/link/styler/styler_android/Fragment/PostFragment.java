package link.styler.styler_android.Fragment;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;

import link.styler.styler_android.Adapter.MyAdapterPosts;
import link.styler.styler_android.ObjectClass.ObjectPosts;
import link.styler.styler_android.R;


public class PostFragment extends ListFragment implements OnItemClickListener
{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<ObjectPosts> arrayObjPosts = new ArrayList<ObjectPosts>();

        ObjectPosts objPost = new ObjectPosts("","Quang Dinh",26,true,"Title","TEST");

        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);
        arrayObjPosts.add(objPost);

        objPost = new ObjectPosts("","Tsubasa Koseki",30,true,"Title 2","1/18の日経産業新聞に弊社のことを掲載いただきましたー 17面のスタートアップ頁です ファッション×ITの文脈で取り上げていただきましたので、興味ある方はキオスクなどでお買い求めください。 また、フォトグラファーの嶌村 吉祥丸くんに写真集ももらいましたー...");
        arrayObjPosts.add(objPost);
        getListView().setAdapter(new MyAdapterPosts(getActivity(),arrayObjPosts));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,long id) {

    }

}

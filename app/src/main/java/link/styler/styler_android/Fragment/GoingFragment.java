package link.styler.styler_android.Fragment;


import android.support.v4.app.ListFragment;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Date;

import link.styler.styler_android.Adapter.MyAdapterGoing;
import link.styler.styler_android.ObjectClass.ObjectGoing;
import link.styler.styler_android.R;


public class GoingFragment extends ListFragment {


    public GoingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("Quang","onCreate");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_going_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<ObjectGoing> arrayObjGoing = new ArrayList<ObjectGoing>();

        ObjectGoing objGoing = new ObjectGoing("","fbh",3,new Date(),"","STYLER");
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);

        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);
        arrayObjGoing.add(objGoing);

        getListView().setAdapter(new MyAdapterGoing(getActivity(),arrayObjGoing));

    }

}

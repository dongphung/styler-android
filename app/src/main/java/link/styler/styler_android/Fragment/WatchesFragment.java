package link.styler.styler_android.Fragment;



import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import link.styler.styler_android.Adapter.MyAdapterWatches;
import link.styler.styler_android.ObjectClass.ObjectWatches;
import link.styler.styler_android.R;


public class WatchesFragment extends ListFragment {


    public WatchesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_watches_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<ObjectWatches> arrayObjWatches = new ArrayList<ObjectWatches>();
        ObjectWatches objWatch = new ObjectWatches("","Yuaya Iwasaki","27",true,"title","イーハトーヴォのすきとおった風、夏でも底に冷たさをも つ青いそら、うつくしい森で飾られたモリーオ市、郊外の ぎらぎらひかる草の波。");

        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);
        arrayObjWatches.add(objWatch);


        getListView().setAdapter(new MyAdapterWatches(getActivity(),arrayObjWatches));
    }
}

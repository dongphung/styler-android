package link.styler.styler_android.Fragment;


import android.support.v4.app.ListFragment;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import link.styler.styler_android.Adapter.MyAdapterFollowings;
import link.styler.styler_android.ObjectClass.ObjectFollowing;
import link.styler.styler_android.R;


public class FollowingFragment extends ListFragment {

    public FollowingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_following_layout, container, false);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<ObjectFollowing> arrayObjFollowings = new ArrayList<ObjectFollowing>();

        ObjectFollowing objFollowing = new ObjectFollowing("","",1,2,4,"takahiro.kawagoe");

        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);
        arrayObjFollowings.add(objFollowing);



        getListView().setAdapter(new MyAdapterFollowings(getActivity(),arrayObjFollowings));

    }
}

package link.styler.styler_android.Fragment;


import android.support.v4.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.util.Log;
import android.widget.ScrollView;

import java.util.ArrayList;

import link.styler.styler_android.Adapter.MyAdapterLikes;
import link.styler.styler_android.ObjectClass.ObjectLikes;
import link.styler.styler_android.R;

public class LikesFragment extends ListFragment {

    private ListView listViewLikes2;
    private View viewLike;
    private ScrollView scrollView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        viewLike = inflater.inflate(R.layout.fragment_likes_layout, container, false);

        listViewLikes2 = (ListView)viewLike.findViewById(R.id.listlike2);

        scrollView  = (ScrollView)viewLike.findViewById(R.id.scrollViewLike);

        return viewLike;
    }

    int t;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);



        //scrollView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,10000));

        ArrayList<ObjectLikes> arrayObjLikes = new ArrayList<ObjectLikes>();
        ObjectLikes objLike             =   new ObjectLikes("","Wonderland","Big shirs","19,440",4);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);

        objLike = new ObjectLikes("","Sasquatchfabrix","LONG SHIRS sjkfgjsflgjslfkgjslgjsflgjlsfgjlsfgjsfglsfjgljsfglfsj","25,920",1);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);
        arrayObjLikes.add(objLike);

        ArrayList<ObjectLikes> arrayObjLikes2 = new ArrayList<ObjectLikes>();
        objLike                         =   new ObjectLikes("","Sasquatchfabrix","LONG SHIRS sjkfgjsflgjslfkgjslgjsflgjlsfgjlsfgjsfglsfjgljsfglfsj","25,920",1);
        arrayObjLikes2.add(objLike);
        objLike                         =   new ObjectLikes("","Wonderland","Big shirs","19,440",4);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);
        arrayObjLikes2.add(objLike);


        getListView().setAdapter(new MyAdapterLikes(getActivity(),arrayObjLikes));
        getListView().setEnabled(false);

        listViewLikes2.setAdapter(new MyAdapterLikes(getActivity(),arrayObjLikes2));
        listViewLikes2.setEnabled(false);

        setListViewHeightBasedOnChildren(getListView());
        setListViewHeightBasedOnChildren(listViewLikes2);


    }

    private void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        int max = 0;
        if(listAdapter == null)
            return;

        int totlalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for(int i = 0;i<listAdapter.getCount();i++)
        {
            View listItem = listAdapter.getView(i,null,listView);
            if(listItem instanceof ViewGroup)
            {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            listItem.measure(0,0);
            if(listItem.getMeasuredHeight() > max)
                max = listItem.getMeasuredHeight();
            Log.i("Quang","max " + max);
            totlalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totlalHeight + (listView.getDividerHeight() * (listAdapter.getCount() -1 )) +  max /2;
        listView.setLayoutParams(params);
    }

    private static final int UNBOUNDED = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

    private int getItemHeighttoListView(ListView listView,int items)
    {
        ListAdapter adapter = listView.getAdapter();
        int grossElementHeight = 0;
        for (int i = 0; i < items; i++) {
            View childView = adapter.getView(i, null, listView);
            childView.measure(UNBOUNDED, UNBOUNDED);
            grossElementHeight += childView.getMeasuredHeight();
        }

        //grossElementHeight += listView.getDividerHeight() * listView.getCount();

        return grossElementHeight;
    }

}

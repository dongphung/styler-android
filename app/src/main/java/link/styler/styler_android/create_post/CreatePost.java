package link.styler.styler_android.create_post;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import link.styler.styler_android.R;

public class CreatePost extends AppCompatActivity {

    TextView txtCategory;
    TextView txtCount;
    TextView txtDone;
    EditText txtPlaceHolder;
    LinearLayout ll1;
    LinearLayout ll2;
    ListView lvCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_post);
        addControls();
        addEvents();
    }

    private void addControls() {
        txtCategory = (TextView) findViewById(R.id.textView2);
        txtCount = (TextView) findViewById(R.id.txtCount);
        txtPlaceHolder = (EditText) findViewById(R.id.txtPlaceHolder);
        ll1 = (LinearLayout) findViewById(R.id.llCreatePost1);
        ll2 = (LinearLayout) findViewById(R.id.llCreatePost2);
        txtDone = (TextView) findViewById(R.id.txtDone);
        lvCategory = (ListView) findViewById(R.id.lvCategory);
        addDatalvCategory(); // <=============== add data list view

    }
    private void addEvents() {

        txtCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showListCategory();
            }
        });
        txtDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideListCategory();
            }
        });
        txtPlaceHolder.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                coutCharacters();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        lvCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String s = (String) lvCategory.getAdapter().getItem(position);
                txtCategory.setText(s);
            }
        });

    }
    //// TODO: 1/19/17  addDatalvCategory CreatePost
    private void addDatalvCategory() {
        final String arr[]={"1111111","222222","333333","1111111","222222","333333","1111111","222222","333333","1111111","222222","333333",
                "1111111","222222","333333","1111111","222222","333333","1111111","222222","333333","1111111","222222","333333",
                "1111111","222222","333333","1111111","222222","333333","1111111","222222","333333","1111111","222222","333333",
                "1111111","222222","333333","1111111","222222","333333"};
        ArrayAdapter<String> adapter=new ArrayAdapter<String>
                (this, android.R.layout.simple_list_item_1, arr);
        lvCategory.setAdapter(adapter);
    }
    private void coutCharacters() {
        int i = 100 - txtPlaceHolder.getText().length();
        txtCount.setText("残り"+ i +"文字");
        if (i<0)
            txtCount.setTextColor(0xFFF0405A);
        else
            txtCount.setTextColor(0xFF007AFF);
    }
    private void hideListCategory() {
        ll1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
    }
    private void showListCategory() {
        int i = ll2.getHeight();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        ll1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,size.y-i));
        // hide
      InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(txtPlaceHolder.getWindowToken(), 0);

    }
}

package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import android.util.Log;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.PersonalProfile;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/16/17.
 */

public class MyAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<PersonalProfile> personalProfile;


    public MyAdapter(Context context, ArrayList<PersonalProfile> personalProfile)
    {
        Log.i("Quang","My Adapter .....");
        this.context = context;
        this.personalProfile = personalProfile;
    }

    @Override
    public int getCount() {
        return personalProfile.size();
    }

    @Override
    public Object getItem(int position)
    {
        return personalProfile.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int postion, View converView, ViewGroup parent)
    {
        Log.i("Quang","My Adapter getView .....");
        if(converView ==  null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            converView = (View)inflater.inflate(R.layout.list_item,null);
        }

        Log.i("Quang","persion " +personalProfile.get(postion).PersonalProfile_PathImageAvatar());

        TextView title = (TextView)converView.findViewById(R.id.txttitle);
        title.setText(personalProfile.get(postion).PersonalProfile_GetTitle());

        ImageView imageViewAvatar = (ImageView)converView.findViewById(R.id.list_imageViewAvatar);
        //imageViewAvatar.setMaxHeight(50);
        //imageViewAvatar.setMaxWidth(50);


        TextView name = (TextView)converView.findViewById(R.id.list_txt_name);
        name.setText(personalProfile.get(postion).PersonalProfile_GetName());


        TextView desc = (TextView)converView.findViewById(R.id.list_txt_desc);
        desc.setText(personalProfile.get(postion).PersonalProfile_GetDescription());

        TextView price = (TextView)converView.findViewById(R.id.list_txt_price);
        price.setText("¥" + personalProfile.get(postion).PersonalProfile_GetPrice());

        ImageView imageView = (ImageView)converView.findViewById(R.id.list_imageView);
        //imageView.setMaxHeight(100);
        //imageView.setMaxWidth(100);

        return converView;
    }


 }

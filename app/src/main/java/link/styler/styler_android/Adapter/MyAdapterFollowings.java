package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectFollowing;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/19/17.
 */

public class MyAdapterFollowings extends BaseAdapter {

    private Context context;
    private ArrayList<ObjectFollowing> arrayObjFollowings;

    public MyAdapterFollowings(Context context, ArrayList<ObjectFollowing> arrayObjFollowings)
    {
        this.context = context;
        this.arrayObjFollowings = arrayObjFollowings;
    }

    @Override
    public int getCount() {
        return arrayObjFollowings.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjFollowings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_following_layout,null);
        }

        TextView txtNumberReply = (TextView)convertView.findViewById(R.id.txtNumberReply);
        txtNumberReply.setText(arrayObjFollowings.get(position).GetNumberReply() + "");

        TextView txtNumberFollow = (TextView)convertView.findViewById(R.id.txtNumberFollow);
        txtNumberFollow.setText(arrayObjFollowings.get(position).GetNumberFollow() + "");

        TextView txtNumberLike = (TextView)convertView.findViewById(R.id.txtNumberLikes);
        txtNumberLike.setText(arrayObjFollowings.get(position).GetNumberLike() + "");

        TextView txtName = (TextView)convertView.findViewById(R.id.txtNameFollowing);
        txtName.setText(arrayObjFollowings.get(position).GetTxtName());

        return convertView;
    }
}

package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectWatches;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/19/17.
 */

public class MyAdapterWatches extends BaseAdapter{

    private Context context;
    private ArrayList<ObjectWatches> arrayObjWatches;

    public MyAdapterWatches(Context context, ArrayList<ObjectWatches> arrayObjWatches)
    {
        this.context = context;
        this.arrayObjWatches = arrayObjWatches;
    }

    @Override
    public int getCount() {
        return arrayObjWatches.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjWatches.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_watch_layout,null);
        }

        TextView txtName = (TextView)convertView.findViewById(R.id.txtNameWatch);
        txtName.setText(arrayObjWatches.get(position).GetTxtName());

        TextView txtAge  = (TextView)convertView.findViewById(R.id.txtAgeWatch);
        txtAge.setText(arrayObjWatches.get(position).GetTxtAge());

        TextView txtSex  = (TextView)convertView.findViewById(R.id.txtSexWatch);
        txtSex.setText(arrayObjWatches.get(position).GetIsSex() ? "Male" : "Female");

        TextView txtTitle = (TextView)convertView.findViewById(R.id.txtTitleWatch);
        txtTitle.setText(arrayObjWatches.get(position).GetTxtTile());

        TextView txtContentWatch = (TextView)convertView.findViewById(R.id.txtContentWatch);
        txtContentWatch.setText(arrayObjWatches.get(position).GetTxtConentWatch());

        return convertView;
    }
}

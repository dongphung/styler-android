package link.styler.styler_android.Adapter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectGoing;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/19/17.
 */

public class MyAdapterGoing extends BaseAdapter {

    private Context context;
    private ArrayList<ObjectGoing> arrayObjGoing;

    public MyAdapterGoing(Context context, ArrayList<ObjectGoing> arrayObjGoing)
    {
        this.context = context;
        this.arrayObjGoing = arrayObjGoing;
    }

    public int getCount() {
        return arrayObjGoing.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjGoing.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_going_layout,null);
        }

        ImageView imaViewImage = (ImageView)convertView.findViewById(R.id.imaImageGoing);
        //get bitmap of the image
        Bitmap imageBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.com_facebook_profile_picture_blank_square);
        RoundedBitmapDrawable roundDrawable = RoundedBitmapDrawableFactory.create(context.getResources(),imageBitmap);
        //roundDrawable.setCircular(true);
        roundDrawable.setCornerRadius(Math.min(roundDrawable.getMinimumWidth(),roundDrawable.getMinimumHeight())/2.0f);
        roundDrawable.setAntiAlias(true);
        imaViewImage.setImageDrawable(roundDrawable);



        TextView txtTitel = (TextView)convertView.findViewById(R.id.txtTitleGoing);
        txtTitel.setText(arrayObjGoing.get(position).GetTxtTitle());

        TextView txtNumberGoing = (TextView)convertView.findViewById(R.id.txtNumberGoing);
        txtNumberGoing.setText(arrayObjGoing.get(position).GetNumberGoing() +"");

        TextView txtDate = (TextView)convertView.findViewById(R.id.txtDateGoing);
        txtDate.setText(arrayObjGoing.get(position).GetDate());

        TextView txtName = (TextView)convertView.findViewById(R.id.txtNameGoing);
        txtName.setText(arrayObjGoing.get(position).GetTxtName());

        return convertView;
    }
}

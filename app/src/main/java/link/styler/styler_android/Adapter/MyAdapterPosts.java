package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.TextView;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectPosts;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/18/17.
 */

public class MyAdapterPosts extends BaseAdapter {

    private ArrayList<ObjectPosts> arrayObjPosts ;
    private Context context;

    public MyAdapterPosts(Context context, ArrayList<ObjectPosts> arrayObjPosts)
    {
        this.context = context;
        this.arrayObjPosts = arrayObjPosts;
    }

    @Override
    public int getCount() {
        return arrayObjPosts.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjPosts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_post_layout,null);
        }

        TextView txtName = (TextView)convertView.findViewById(R.id.txtNamePost);
        txtName.setText(arrayObjPosts.get(position).GetTxtName());

        TextView txtAge = (TextView)convertView.findViewById(R.id.txtAgePost);
        txtAge.setText(arrayObjPosts.get(position).GetAge() + "");

        TextView txtSex = (TextView)convertView.findViewById(R.id.txtSexPost);
        txtSex.setText(arrayObjPosts.get(position).GetIsSex() ? "Male" : "Female");

        TextView txtTile = (TextView)convertView.findViewById(R.id.txtTitlePost);
        txtTile.setText(arrayObjPosts.get(position).GetTxtTile());

        TextView txtTextPost = (TextView)convertView.findViewById(R.id.txtTextPost);
        txtTextPost.setText(arrayObjPosts.get(position).GetTxtPost());

        return convertView;
    }
}

package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.Button;
import android.widget.TextView;
import android.util.Log;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectLikes;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/18/17.
 */

public class MyAdapterLikes extends BaseAdapter {

    private ArrayList<ObjectLikes> arrayObjLikes;
    private Context context;

    public MyAdapterLikes(Context context,ArrayList<ObjectLikes> arrayObjLikes)
    {
        this.context = context;
        this.arrayObjLikes = arrayObjLikes;
    }

    @Override
    public int getCount() {
        return arrayObjLikes.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjLikes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_like_layout,null);
        }

        //ImageView pathImaAvatar = (ImageView)convertView.findViewById(R.id.imViewLike);

        TextView txtTitle       = (TextView)convertView.findViewById(R.id.txtTitleLike);
        txtTitle.setText(arrayObjLikes.get(position).GetTxtTitle());

        TextView txtName       = (TextView)convertView.findViewById(R.id.txtNameLike);
        txtName.setText(arrayObjLikes.get(position).GetTxtName());

        TextView txtPrice      = (TextView)convertView.findViewById(R.id.txtPriceLike);
        txtPrice.setText(arrayObjLikes.get(position).GetTxtPrice());

        TextView txtNumberLike  = (TextView)convertView.findViewById(R.id.txtNumberLike);
        txtNumberLike.setText(arrayObjLikes.get(position).GetNumberLike() + "Likes");

        Button btnDiskLike      = (Button)convertView.findViewById(R.id.btnDiskLike);
        btnDiskLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Quang","array list Like "+arrayObjLikes.get(position).GetTxtName());
                //arrayObjLikes.remove(position);
            }
        });


        return convertView;
    }
}

package link.styler.styler_android.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.Switch;
import android.widget.TextView;
import android.util.Log;

import java.util.ArrayList;

import link.styler.styler_android.ObjectClass.ObjectSettings;
import link.styler.styler_android.R;

/**
 * Created by admin on 1/17/17.
 */

public class MyAdapterSetting extends BaseAdapter {

    private Context context;
    private ArrayList<ObjectSettings>  arrayObjectSettings;

    public MyAdapterSetting(Context context, ArrayList<ObjectSettings> arrayObjectSettings)
    {
        this.context = context;
        this.arrayObjectSettings   = arrayObjectSettings;
    }

    @Override
    public int getCount() {
        return arrayObjectSettings.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayObjectSettings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup nt) {
        if(convertView == null)
        {
            LayoutInflater inflater =  (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View)inflater.inflate(R.layout.list_setting_layout,null);
        }

        TextView txtview = (TextView)convertView.findViewById(R.id.txt_title_setting);
        txtview.setText(arrayObjectSettings.get(position).GetTitle().toString());

        txtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(arrayObjectSettings.get(position).GetNextActivity().toString() == null)
                {
                    Log.i("Quang","NULLLLLLLLL");

                }
                else
                    Log.i("Quang","" +arrayObjectSettings.get(position).GetNextActivity().toString());
            }
        });

        Switch swSetting = (Switch)convertView.findViewById(R.id.sw_setting);
        if(arrayObjectSettings.get(position).IsSwitchButton()) {
            swSetting.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Is the switch is on
                    boolean on = ((Switch) v).isChecked();
                    if (on) {
                        //On
                    } else {
                        //Off
                    }
                }
            });
        }
        else
            swSetting.setVisibility(View.INVISIBLE);

        return convertView;
    }
}

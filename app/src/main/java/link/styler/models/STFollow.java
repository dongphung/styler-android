package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STFollow extends RealmObject{
    @PrimaryKey
    public int      followID    =0;
    public int      userID      =0;
    public int      staffID     =0;
    public String   createdAt   ="";
    public STUser   user        = new STUser();
    public STUser   staff       = new STUser();

    public void fillInfo(JSONObject info, STUser user) throws JSONException {
        this.followID   = info.getInt("id");
        this.userID     = info.getInt("user_id");
        this.staffID    = info.getInt("staff_id");
        this.createdAt  = info.getString("created_at");
        // phân biệt user & staff  bằng cách nào????????????????????????
        if (userID != 0 && staffID ==0)
            this.user   = user;
        else if ( staffID !=0)
            this.staff  = user;
    }
}

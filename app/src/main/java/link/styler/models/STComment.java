package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STComment extends RealmObject {
    @PrimaryKey
    public int      commentID   = 0;
    public int      postID      = 0;
    public STUser   user        = new STUser();
    public STItem   item        = new STItem();
    public String   text        = "";
    public String   createdAt   = "";

    public void fillInfo(JSONObject info) throws JSONException {
        this.commentID  = info.getInt("id");
        this.postID     = info.getInt("post_id");
        this.text       = info.getString("text");
        this.createdAt  = info.getString("created_at");
    }
}

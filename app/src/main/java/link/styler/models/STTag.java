package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/9/2017.
 */
public class STTag extends RealmObject {
    @PrimaryKey
    public int      tagID   = 0;
    public String   name    = "";
}

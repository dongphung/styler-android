package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
public class STItem extends RealmObject {
    @PrimaryKey
    public int          itemID      = 0;
    public int          shopID      = 0;
    public STShop       shop        = new STShop();
    public String       text        = "";
    public STImage      mainImage   = new STImage();
     public RealmList<STImageURL>   imageURLs   = new RealmList<>();
    public int          likeCount   = 0;
    public String       brand       = "";
    public String       name        = "";
    public String       price       = "";
    public int          likeID      = 0;
    public int          categoryID  = 0;
    public STCategory   category  = new STCategory();
     public RealmList<STReply>      replies    = new RealmList<>();
    public String       createdAt   = "";

    public void fillInfo(Realm realm, JSONObject info) throws JSONException, IOException {
        this.itemID         = info.getInt("id");
        this.shopID         = info.getInt("shop_id");
        this.text           = info.getString("text");

        if (info.has("images_url"))
        {
            final JSONObject imagesURL = info.getJSONObject("images_url");
            if(imagesURL.has("image_url_1")){
                final String mainImageURL =  imagesURL.getString("image_url_1");
                STImage image = new STImage();
                image.fillByURL(realm, mainImageURL);
                mainImage = image;
            }
            saveImages(realm,itemID,imagesURL);
        }
        //let imageURLs = realm.objects(STImageURL).filter("itemID=%@", info["id"] as? Int ?? 0)
        //self.imageURLs.appendContentsOf(imageURLs)
        // chuyển 2 line trên vào trong saveImages

        this.likeCount      = info.getInt("likes_count");
        this.brand          = info.getString("brand");
        this.name           = info.getString("name");
        if (info.has("price"))
            this.price = info.getInt("price") +"";
        this.likeID         = info.getInt("like_id");
        this.categoryID     = info.getInt("category_id");
        this.createdAt      = info.getString("created_at");
        // TODO: 1/6/2017  // STItem.swift //self.category   = DataLoader.getCategory(self.categoryID)
        this.category       = null;
    }

    private void saveImages(Realm realm, int itemID,JSONObject imagesURL) throws JSONException {
        Iterator<String> keys1 = imagesURL.keys();
        ArrayList<String> keys = new ArrayList<String>();
        while (keys1.hasNext()){
            keys.add(keys1.next());
        }
        for (String key : keys){
            if(imagesURL.has(key)){
                final String imageURL = imagesURL.getString(key);
                if (imageURL.length()>0){
                    int order = 1;
                    switch (key){
                        case "image_url_1":
                            order = 1;
                            break;
                        case "image_url_2":
                            order = 2;
                            break;
                        case "image_url_3":
                            order = 3;
                            break;
                        case "image_url_4":
                            order = 4;
                            break;
                        default:
                            order = 1;
                            break;
                    }
                    STImageURL stImageURL = new STImageURL();
                    stImageURL.fillInfoByItem(imageURL, itemID, order);
                    this.imageURLs.add(stImageURL); //<= 2 line here =============
                }
            }
        }
    }
    //List<STReply> replies // code switf:  replies: Results<STReply>  // <============
    public void fillAllInfo(Realm realm, JSONObject info, STShop shop, RealmResults<STReply> replies, STCategory category) throws JSONException, IOException {
        fillInfo(realm, info);
        this.shop       = shop;
        this.replies.addAll(replies);
        this.category   = category;
    }
}
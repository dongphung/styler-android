package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */
public class STShop extends RealmObject {
    @PrimaryKey
    public int      shopID      = 0;
    public int      locationID  = 0;
    public String   name        = "";
    public String   station     = "";
    public String   profile     = "";
    public String   address     = "";
    public String   tel         = "";
    public String   website     = "";
    public String   businessHour= "";
    public String   imageURL    = "";
    public int      itemCount   = 0;
    public int      eventCount  = 0;;
    public int      staffCount  = 0;;
    public String   createdAt   = "";

    public void fillInfo(Realm realm, JSONObject info) throws JSONException {
        this.shopID     = info.getInt("id");
        this.locationID = info.getInt("location");
        this.name       = info.getString("name");
        this.station    = info.getString("station");
        this.address    = info.getString("address");
        this.tel        = info.getString("tel");
        this.website    = info.getString("website");
        this.businessHour = info.getString("business_hours");
        this.profile    = info.getString("profile");
        this.imageURL   = info.getString("image_url");
        this.itemCount  = info.getInt("items_count");
        this.eventCount = info.getInt("events_count");
        this.staffCount = info.getInt("staffs_count");
        this.createdAt  = info.getString("created_at");
    }
}

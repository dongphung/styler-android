package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STConversation extends RealmObject{
    @PrimaryKey
    public int     conversationID = 0;
    public String  newestText = "";
    public boolean unreadFlag = false;
    public String  newestTime = "";
    public int     destinationID = 0;
    public String  destinationName = "";
    public String  destinationImageURL = "";
     public RealmList<STMessage> messages = new RealmList<>();

    public void fillInfo(JSONObject info) throws JSONException {
        this.conversationID         = info.getInt("id");
        this.newestText             = info.getString("newest_text");
        this.unreadFlag             = info.getBoolean("unread_flag");
        this.newestTime             = info.getString("newest_time");
        this.destinationID          = info.getInt("destination_id");
        this.destinationName        = info.getString("destination_name");
        this.destinationImageURL    = info.getString("destination_image_url");
    }

    public void fillAllInfo(JSONObject info,RealmList<STMessage> messages) throws JSONException {
        fillInfo(info);
       this.messages = messages;
    }
}

package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/9/2017.
 */
enum ArticleSortType {
    None("none"),
    Timeline("timeline"),
    Categorized("categorized"),
    Feature("feature"),
    Tagged("tagged");
    private String articleSortType;
    private ArticleSortType(String articleSortType){
        this.articleSortType = articleSortType;
    }

    public String getArticleSortType() {
        return articleSortType;
    }
}

public class STArticleSort extends RealmObject {
    @PrimaryKey
    public String       articleSortKey  = "";
    public int          articleID       = 0;
    public STArticle    article         = new STArticle();
    public int          categoryID      = 0;
    public int          tagID           = 0;
    public String       type            = ArticleSortType.None.getArticleSortType();
    public String       publishedAt     = "";
}

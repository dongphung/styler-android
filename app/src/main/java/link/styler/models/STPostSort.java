package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */
enum PostSortType {
    None("none"),
    Popular("popular"),
    Latest("latest"),
    Recommend("recommend"),
    Watch("watch"),
    User("user"),
    Unreplied("unreplied");

    private String postSortType;
    private PostSortType(String postSortType){
        this.postSortType = postSortType;
    }

    public String getPostSortType() {
        return postSortType;
    }
}

public class STPostSort extends RealmObject {
    @PrimaryKey
    public String   postSortKey = "";
    public int      newestTime  = 0;
    public String   type        = PostSortType.None.getPostSortType();
    public int      postID      = 0;
    public int      userID      = 0;
    public int      categoryID  = 0;
    public STPost   post        = new STPost();
}

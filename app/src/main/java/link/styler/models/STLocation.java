package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STLocation extends RealmObject {
    @PrimaryKey
    public int      locationID  = 0;
    public String   name        = "";
    public int      count       = 0;
    public boolean  newShopFlag = false;

    public void fillInfo(JSONObject info) throws JSONException {
        this.locationID = info.getInt("id");
        this.name       = info.getString("name");
        if (this.name == "未設定" ){
            this.name = "オンライン";
        }
        this.count      = info.getInt("count");
        this.newShopFlag= info.getBoolean("new_shop_flag");
    }
}

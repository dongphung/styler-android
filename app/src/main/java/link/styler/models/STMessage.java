package link.styler.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */

enum MessageType {
    None("none"),
    User("user"),
    Link("link"),
    Reply("reply"),
    Notification("notification");

    private String messageType;
    private MessageType(String messageType){
        this.messageType = messageType;
    }

    public String getMessageType() {
        return messageType;
    }
}

public class STMessage  extends RealmObject{
    @PrimaryKey
    public int          messageID           = 0;
    public int          userID              = 0;
    public int          conversationID      = 0;
    public int          destinationUserID   = 0;
    public String       text                = "";
    public String       type                = MessageType.None.getMessageType(); // <=>code swift: MessageType.None.rawValue
    public String       createdAt           = "";
    // code swift: dynamic var userImage: NSData? = nil // dùng bitmap thay thế để set image ????????????????
    public byte[]       userImage;
    //link
    public String       imageURL            = "";
    public String       linkURL             = "";
    public String       title               = "";
    public String       linkDescription     = "";
    //Item
    public STItemSort   itemSort            = new STItemSort();

    public void fillInfo(JSONObject info) throws JSONException, IOException {
        this.messageID      = info.getInt("id");
        this.userID         = info.getInt("user_id");
        this.conversationID = info.getInt("conversation_id");
        this.text           = info.getString("text");
        URL userImageURL = new URL(info.getString("user_image_url")) ;
        if (userImageURL != null)
        {
            HttpURLConnection connection = (HttpURLConnection) userImageURL.openConnection();
            final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            userImage = stream.toByteArray();
        }
        switch (info.getString("type")){
            case "UserMessage":
                this.type = MessageType.User.getMessageType();
                break;
            case "LinkMessage":
                this.type = MessageType.Link.getMessageType();
                if(info.has("objects")){
                    JSONObject object = info.getJSONObject("objects");
                    this.imageURL   = object.getString("image_url");
                    this.linkURL    = object.getString("url");
                    this.title      = object.getString("title");
                    this.linkDescription    = object.getString("description");
                }
                break;
            case "ReplyMessage":
                this.type = MessageType.Reply.getMessageType();
                break;
            case "NotificationMessage":
                this.type = MessageType.Notification.getMessageType();
                break;
            case "":
                this.type = MessageType.None.getMessageType();
                break;
            default:
                this.type = MessageType.None.getMessageType();
                break;
        }
        this.createdAt = info.getString("created_at");
    }

    public void fillAllInfo(JSONObject info, STItemSort itemSort) throws IOException, JSONException {
        fillInfo(info);
        this.itemSort = itemSort;
    }
}

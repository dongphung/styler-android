package link.styler.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

public class STEvent extends RealmObject {
    @PrimaryKey
    public int      eventID         = 0;
    public int      userID          = 0;
    public int      shopID          = 0;
    public int      interestID      = 0;
    public int      interestsCount  = 0;
    public String   title           = "";
    public String   text            = "";
    // code swift:   dynamic var imageData: NSData? = nil // change NSdata <======
    public byte[]   imageData       = null;
    public String   imageURL        = "";
    public String   location        = "";
    public String   startDate       = "";
    public String   finishDate      = "";
    public STShop   shop            = new STShop();
     public RealmList<STUserSort> interestUsers = new RealmList<>();

    public void fillInfo(Realm realm, JSONObject info, int shopID) throws JSONException, IOException {
        this.eventID        = info.getInt("id");
        this.userID         = info.getInt("user_id");
        this.shopID         = shopID;
        this.interestID     = info.getInt("interest_id");
        this.interestsCount = info.getInt("interests_count");
        this.title          = info.getString("title");
        this.text           = info.getString("text");
        this.imageURL       = info.getString("image_url");

        URL url = new URL(this.imageURL);
        if (url != null)
        {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            final Bitmap bitmap = BitmapFactory.decodeStream(connection.getInputStream());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
            imageData = stream.toByteArray();
        }
        this.location       = info.getString("location");
        this.startDate      = info.getString("start_date");
        this.finishDate     = info.getString("finish_date");
    }
}

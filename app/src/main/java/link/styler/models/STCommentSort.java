package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */


enum CommentSortType{
    None("none"),
    Post("post"),
    Article("article"),
    Comment("comment");
    private String commentSortType;
    private CommentSortType(String commentSortType){
        this.commentSortType = commentSortType;
    }

    public String getCommentSortType() {
        return commentSortType;
    }
}

public class STCommentSort extends RealmObject{
    @PrimaryKey
    public String       commentSortKey  = "";
    public String       type            = CommentSortType.None.getCommentSortType();
    public int          postId          = 0;
    public int          articleId       = 0;
    public int          commentID       = 0;
    public STComment    comment         = new STComment();
    public STReply      reply           = new STReply();

}

package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
enum NotificationType{
    Comment(1),Event(2),Feedback(3),Follow(4),Interest(5),Like(6),Reply(7),;
    private int notificationType;
    private NotificationType(int notificationType){
        this.notificationType = notificationType;
    }
    public int getNotificationType() {
        return notificationType;
    }
}
public class STNotification extends RealmObject {

    //Common
    @PrimaryKey
    public int      notificationID  = 0;
    public int      type            = 0;
    public int      authorID        = 0;
    public String   authorName      = "";
    public String   authorImageURL  = "";
    public String   notificationText= "";

    //Comment
    public int      commentID       = 0;
    public String   commentText     = "";
    public int      postID          = 0;

    //FeedBack
    public int      feedbackRate    = 0;

    //Item
    public int      itemID          = 0;
    public String   itemBrand       = "";
    public String   itemName        = "";
    public int      itemPrice       = 0;
    public String   itemImageURL    = "";

    //Event
    public int      eventID         = 0;
    public String   eventTitle      = "";
    public String   eventStartDate  = "";
    public String   eventFinishDate = "";
    public String   eventImageURL   = "";

    //Watch
    //postID: int
    public String   postText        = "";

    public void fillInfo(Realm realm, JSONObject info) throws JSONException {
        //Common
        this.notificationID = info.getInt("notification_id");
        if(info.has("notification_type"))
            this.type           = info.getInt("notification_type");
        else
            this.type           = 1;
        this.authorID       = info.getInt("author_id");
        this.authorName     = info.getString("author_name");
        this.authorImageURL = info.getString("author_image_url");
        this.notificationText = info.getString("notification_text");

        //Comment
        this.commentID      = info.getInt("comment_id");
        this.commentText    = info.getString("comment_text");
        this.postID         = info.getInt("post_id");

        //FeedBack
        this.feedbackRate   = info.getInt("feedback_rate");

        //Item
        this.itemID         = info.getInt("item_id");
        this.itemBrand      = info.getString("item_brand");
        this.itemName       = info.getString("item_name");
        this.itemPrice      = info.getInt("item_price");
        this.itemImageURL   = info.getString("item_image_url");

        //Event
        this.eventID        = info.getInt("event_id");
        this.eventTitle     = info.getString("event_title");
        this.eventStartDate = info.getString("event_start_date");
        this.eventFinishDate= info.getString("event_finish_date");
        this.eventImageURL  = info.getString("event_image_url");

        //Watch
        this.postText       = info.getString("post_text");
    }
}

package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/5/2017.
 */

public class STReply extends RealmObject {
    @PrimaryKey
    public int      replyID         = 0;
    public int      itemID          = 0;
    public int      postID          = 0;
    public int      conversationID  = 0;
    public String   comment         = "";
    public String   createdAt       = "";
    public boolean  feedbackFlag    = false;
    public STShop   shop            = new STShop();
    public STUser   user            = new STUser();

    public void fillInfo(JSONObject info, int itemID) throws JSONException {
        this.replyID            = info.getInt("id");
        this.itemID             = itemID;
        this.postID             = info.getInt("post_id");
        this.comment            = info.getString("comment");
        this.createdAt          = info.getString("created_at");
        this.conversationID     = info.getInt("conversation_id");
        this.feedbackFlag       = info.getBoolean("feedback_flag");
    }

    public void update(JSONObject info) throws JSONException {
        comment         = info.getString("comment");
        createdAt       = info.getString("created_at");
        conversationID  = info.getInt("conversation_id");
        feedbackFlag    = info.getBoolean("feedback_flag");
    }

    public void fillAllInfo(JSONObject info, int itemID, STShop shop, STUser user) throws JSONException {
        fillInfo(info,itemID);
        this.shop = shop;
        this.user = user;
    }
}

package link.styler.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/6/2017.
 */

enum UserSortType{
    None("none"),
    Following("following"),
    Follower("follower"),
    Staff("staff"),
    Interest("interest");

    private String userSortType;
    private UserSortType(String userSortType){
        this.userSortType = userSortType;
    }

    public String getUserSortType() {
        return userSortType;
    }
}

public class STUserSort extends RealmObject {
    @PrimaryKey
    public String   userSortKey = "";
    public String   type        = UserSortType.None.getUserSortType();
    public int      userID      = 0;
    public STUser   user        = new STUser();
    public int      shopID      = 0;
    public int      eventID     = 0;
    public int      targetUserID= 0;
}

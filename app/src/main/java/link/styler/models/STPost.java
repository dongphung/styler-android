package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


/**
 * Created by Tan Nguyen on 1/4/2017.
 */

public class STPost extends RealmObject {
    @PrimaryKey
    public int      postID          = 0;
    public STUser   user            = new STUser();
    public int      commentsCount   = 0;
    public int      watchesCount    = 0;
    public int      repliesCount    = 0;
    public String   text            = "";
    public int      watchID         = 0;
    public int      replyID         = 0;
     public RealmList<STItemSort>   itemSorts     = new RealmList<>();
     public RealmList<STImageURL>   itemImageURLs = new RealmList<>();
    public int      categoryID      = 0;
    public STCategory category      = new STCategory();
    public String   createdAt       = "";
    public String   newestTime      = "";

    public void fillInfo(Realm realm, JSONObject info) throws JSONException {
        this.postID         = info.getInt("id");
        this.commentsCount  = info.getInt("comments_count");
        this.watchesCount   = info.getInt("watches_count");
        this.repliesCount   = info.getInt("replies_count");
        this.text           = info.getString("text");
        this.watchID        = info.getInt("watch_id");
        this.replyID        = info.getInt("reply_id");
        this.createdAt      = info.getString("created_at");
        this.newestTime     = info.getString("newest_time");
        this.categoryID     = info.getInt("category_id");
        this.category       = null;// TODO: 1/5/2017// self.category = DataLoader.getCategory(self.categoryID)

        if (info.has("item_images_url")){
            String[] imagesURL = (String[]) info.get("item_images_url");
            saveImages(realm,postID,imagesURL );

            //code swift: add imageURLs vao itemImageURLs
           /* if let imagesURL: [String] = info["item_images_url"] as? [String] {
                saveImages(realm, postID: info["id"] as? Int ?? 0, imagesURL: imagesURL)
            }
            let imageURLs = realm.objects(STImageURL).filter("postID=%@", info["id"] as? Int ?? 0)
            self.itemImageURLs.appendContentsOf(imageURLs)*/

            // chagne:  :(
        }
    }
    private void saveImages(Realm realm, int postID, String[] imagesURL){
        int order = 1;
        for(String imageURL : imagesURL ){
            STImageURL stImageURL = new STImageURL();
            stImageURL.fillInfoByPost(imageURL,postID,order);
            this.itemImageURLs.add(stImageURL);
            order +=1;
        }
    }

    public void fillAllInfo(Realm realm, JSONObject info, STUser user, RealmList<STItemSort>itemSorts, STCategory category ) throws JSONException {
        fillInfo(realm,info);
        this.user = user;
        this.itemSorts = itemSorts;
        this.category = category;
    }
}

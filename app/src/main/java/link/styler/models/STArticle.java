package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/9/2017.
 */

public class STArticle extends RealmObject{
    @PrimaryKey
    public int          articleID   = 0;
    public int          categoryID  = 0;
    public int          authorID    = 0;
    public String       title       = "";
    public String       desc        = "";
    public String       articleURL  = "";
    public String       imageURL    = "";
    public String       publishedAt = "";
    public STCategory   category    = new STCategory();
    public RealmList<STTag> tags = new RealmList<>();

    public void fillInfo(Realm realm, JSONObject info) throws JSONException {
        this.articleID      = info.getInt("id");
        this.categoryID     = info.getInt("category_id");
        this.authorID       = info.getInt("author_id");
        this.title          = info.getString("title");
        this.desc           = info.getString("description");
        this.articleURL     = info.getString("url");
        this.imageURL       = info.getString("thumbnail");
        this.publishedAt    = info.getString("created_at");
        //// TODO: 1/9/2017 // đã xong // STArticle.swift // self.category = realm.objects(STCategory).filter("categoryID=%@", self.categoryID).first
        // đúng ??????
        this.category       = realm.where(STCategory.class).equalTo("categoryID",categoryID).findFirst();
    }
}

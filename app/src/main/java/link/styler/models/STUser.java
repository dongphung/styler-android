package link.styler.models;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Tan Nguyen on 1/4/2017.
 */
enum UserStatus{
    None("none"),Customer("customer"),Staff("staff");
    private String userStatus;
    private UserStatus (String userStatus){
        this.userStatus = userStatus;
    }

    public String getUserStatus() {
        return userStatus;
    }
    static UserStatus checkStatus(String status){
        /*if (status == null)
            return null;*/
        switch (status){
            case "customer":
                return UserStatus.Customer;
            case "staff":
                return UserStatus.Staff;
            default:
                return UserStatus.None;
        }
    }
}

public class STUser extends RealmObject {
    @PrimaryKey
    public int      userID          = 0;
    public String   name            = "";
    public String   apiDetail       = "";
    public String   imageURL        = "";
    public String   profile         = "";
    public int      likeCount       = 0;
    public int      interestCount   = 0;
    public int      postCount       = 0;
    public int      watchCount      = 0;
    public int      followCount     = 0;
    public int      replyCount      = 0;
    public int      followerCount   = 0;
    public int      followID        = 0;
    public int      shopID          = 0;
    public STShop   shop            = new STShop();
    public STUserData userData      = new STUserData();
    public String   location        = "";
    public int      locationID      = 0;
    public String   birthday        = "";
    public String   gender          = "";
    public String   status          = UserStatus.None.getUserStatus();
     public RealmList<STPost> watches= new RealmList<>();
     public RealmList<STPost> posts  = new RealmList<>();

    public void fillInfo(Realm realm, JSONObject info) throws JSONException {
        if (this.userID == 0)
            this.userID = info.getInt("id");
        this.name           = info.getString("name");
        this.apiDetail      = info.getString("api_detail");
        this.profile        = info.getString("profile");
        this.imageURL       = info.getString("image_url");
        this.likeCount      = info.getInt("likes_count");
        this.interestCount  = info.getInt("interests_count");
        this.postCount      = info.getInt("posts_count");
        this.watchCount     = info.getInt("watches_count");
        this.followCount    = info.getInt("follows_count");
        this.replyCount     = info.getInt("replies_count");
        this.followerCount  = info.getInt("followers_count");
        this.followID       = info.getInt("follow_id");
        this.locationID     = info.getInt("location");
        this.location       = "todo : id から locationを取得";
        this.birthday       = info.getString("birthday");
        this.gender         = info.getString("gender");
        this.shopID         = info.getInt("shop_id");
        switch (info.getString("status")){
            case "customer":
                this.status = UserStatus.Customer.getUserStatus();
                break;
            case "staff":
                this.status = UserStatus.Staff.getUserStatus();
                break;
            default:
                this.status = UserStatus.None.getUserStatus();
                break;
        }
    }

    public void fillAllInfo(Realm realm, JSONObject info, STShop shop, STUserData userData) throws JSONException {
        fillInfo(realm,info);
        this.shop       = shop;
        this.userData   = userData;
    }
}

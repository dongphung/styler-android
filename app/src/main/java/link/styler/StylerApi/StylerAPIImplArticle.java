package link.styler.StylerApi;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplArticle {
    public static void getArticle(int articleID, final STCompletion.WithJson completion) {
        String path = "/articles/" + articleID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void getArticles(Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/articles";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjID != null)
            querryString.put("last_object_id", lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getCategoryArticles(int categoryID, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/categories/" + categoryID + "/articles";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjID != null)
            querryString.put("last_object_id", lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getTagArticles(int tagID, Integer lastObjID, final STCompletion.WithJson completion) {
        String path = "/tags/" + tagID + "/articles";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjID != null)
            querryString.put("last_object_id", lastObjID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getTopArticles(STCompletion.WithJson completion) {
        String path = "/articles/top";
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }
}

package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplComment {
    public static void getComments(int postID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void postComment(int postID, String text, Integer itemID, final STCompletion.Base completion) {

    }

    public static void deleteComment(int commentID, final STCompletion.Base completion) {

    }
}

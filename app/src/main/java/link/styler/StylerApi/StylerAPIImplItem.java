package link.styler.StylerApi;

import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplItem {
    public static void getPopularItems(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getItem(int itemID, final STCompletion.WithJson completion) {

    }

    public static void patchItem(int itemID, Map<String, String> param, final STCompletion.Base completion) {

    }
}

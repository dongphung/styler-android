package link.styler.StylerApi;

import android.graphics.Bitmap;

import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplShop {
    public static void getLocations(STCompletion.WithJson completion) {

    }

    public static void getShops(int locationID, String order, Integer lastObjID, final STCompletion.WithJson completion) {

    }

    public static void getShop(int shopID, final STCompletion.WithJson completion) {

    }

    public static void getRepliesOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        // TODO: need to check [STReply] - array instead of json obj
        // TODO: not using on iOS
    }

    public static void getItemsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getItemsOfShopWithCategory(int shopID, int categoryID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getColleagueOfStaff(int staffID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {
        // TODO: need to check [STUser] - array instead of json obj
        // TODO: not using on iOS
    }

    public static void getStaffsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void updateShop(int shopID, Map<String, Object> param, Bitmap photo, final STCompletion.WithJson completion) {

    }
}

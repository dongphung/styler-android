package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplTodo {
    public static void getTodos(Integer lastObjID, final STCompletion.WithNullableInt completion) {

    }

    public static void postTodo(int postID, final STCompletion.Base completion) {

    }

    public static void deleteTodo(int todoID, final STCompletion.Base completion) {

    }
}

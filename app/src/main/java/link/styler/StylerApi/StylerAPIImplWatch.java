package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplWatch {
    public static void getWatches(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void postWatch(int postID, final STCompletion.WithJson completion) {

    }

    public static void deleteWatch(int watchID, final STCompletion.Base completion) {

    }
}

package link.styler.StylerApi;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STStructs;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplReply {
    public static void getRepliesOfPost(int postID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/posts/" + postID + "/replies";
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void createReply(STStructs.StylerReplyStruct replyStruct, final STCompletion.Base completion) {
        String path = "/replies";

        Map<String, Object> httpBody = new HashMap<String, Object>();
        httpBody.put("post_id", replyStruct.postID);
        httpBody.put("comment", replyStruct.comment);
        httpBody.put("item_id", replyStruct.itemID);
        StylerAPIBaseRequests.sendPost(path, httpBody, completion);
    }

    public static void createReplyAndItem(STStructs.StylerReplyAndItemStruct replyAndItemStruct, Bitmap photos, final STCompletion.WithJson completion) {
        // TODO: image uploading
    }

    public static void patchReply(STStructs.StylerUpdateItemAndReplyStruct params, Bitmap photos, final STCompletion.WithJson completion) {
        // TODO: image uploading
    }

    public static void getReply(int replyID, final STCompletion.WithJson completion) {
        String path = "/replies/" + replyID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void deleteReply(int postID, int replyID, final STCompletion.Base completion) {
        String path = "/posts/" + postID + "/replies/" + replyID;
        StylerAPIBaseRequests.sendDelete(path, completion);
    }
}

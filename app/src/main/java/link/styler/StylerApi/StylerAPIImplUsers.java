package link.styler.StylerApi;

import android.graphics.Bitmap;

import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import link.styler.CompletionInterfaces.*;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;
import link.styler.styler_android.Constants;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplUsers {
    public static void getUser(int userID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID;
        StylerAPIBaseRequests.sendGet(path, null, completion);
    }

    public static void postUser(String email, String password, final STCompletion.WithJsonToken completion) {
        String path = "/sign_up";

        Map<String, Object> httpBody = new HashMap<String, Object>();
        httpBody.put("email", email);
        httpBody.put("password", password);
        StylerAPIBaseRequests.sendPost(path, httpBody, completion);
    }

    public static void patchUser(Bitmap photo, String name, int location, String birthday, String  gender, String profile, final STCompletion.WithJson completion) {
        // TODO:
    }

    public static void deleteUser(String userID, final STCompletion.Base completion) {
        //TODO: not using on iOS
    }

    public static void getPostsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/posts";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getWatchesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID +"/watches";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getRepliesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/replies";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getLikesOfUser(int userID, Integer lastObjectID, final STCompletion.WithJson completion) {
        String path = "/users/" + userID +"/likes";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void getInterestsOfUser(int userID, Integer lastObjectID, final STCompletion.WithJsonNullableInt completion) {
        String path = "/users/" + userID +"/replies";
        Map<String, String> querryString = new HashMap<String, String>();
        if(lastObjectID != null)
            querryString.put("last_object_id", lastObjectID.toString());
        StylerAPIBaseRequests.sendGet(path, querryString, completion);
    }

    public static void postDeviceToken(STCompletion.Base completion) {
        String path = "/token_reference";

        Map<String, String> params = new HashMap<String, String>();
        params.put("version_num", Constants.appVersion());

        // TODO: device token
        //if()
        //    params.put("device_token", )

        //StylerAPIBaseRequests.sendPost(path, params, completion);
    }
}

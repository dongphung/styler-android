package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplNotification {
    public static void checkNewNotification(STCompletion.Base completion) {

    }

    public static void getActivities(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getMessageList(Integer lastObjID, final STCompletion.WithNullableInt completion) {
        //TODO: not using on iOS
    }
}

package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplSetting {
    public static void sendNotificationSetting(boolean pushNotification, boolean mailNotification, boolean mailMagazine) {

    }

    public static void sendEmail(String email, final STCompletion.Base completion) {

    }

    public static void sendPassword(String currentPassword, String newPassword, String confirmPassword, final STCompletion.Base completion) {

    }
}

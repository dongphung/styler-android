package link.styler.StylerApi;

import android.app.Activity;
import android.util.Log;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import cz.msebera.android.httpclient.Header;
import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;
import link.styler.styler_android.Constants;

/**
 * Created by dongphung on 1/17/17.
 */

class StylerAPIBaseRequests {
    private static WeakReference<Activity> currentActivity;

    private static AsyncHttpClient requestClient = new AsyncHttpClient();

    public static void setCurrentActivity(Activity activity) {
        currentActivity = new WeakReference<Activity>(activity);
    }

    public  static String urlWithPath(String path, Map<String, String> param) {
        String urlString = Constants.apiUrl(path);
        String query = "";
        if (param != null) {
            for (Map.Entry<String, String> entry: param.entrySet()) {
                if (!query.isEmpty()) {
                    query += "&";
                }
                query += entry.getKey() + "=" + entry.getValue();
            }
        }

        if (!query.isEmpty()) {
            urlString += "?" + query;
        }

        return urlString;
    }

    public static void startRequest(STURLRequest request, final STCompletion.WithJsonInt callback) {
        requestClient.removeAllHeaders();
        requestClient.addHeader("X-STYLER-APP-VERSION", Constants.appVersion());
        if (StylerApi.getAccessTokenBlock != null)
            requestClient.addHeader("X-STYLER-TOKEN", StylerApi.getAccessTokenBlock);

        AsyncHttpResponseHandler respondhandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                if (response == null ) {
                    Log.e("[STyler Error]", "Can not parse respond into JSON object");
                    callback.onRequestComplete(null, false, new STError("Could not parse json from respond" + headers.toString()), statusCode);
                    return;
                }

                callback.onRequestComplete(response, true, null, statusCode);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray timeline) {
                try {
                    //JSONObject firstEvent = (JSONObject) timeline.get(0);
                    callback.onRequestComplete(new JSONObject(timeline.toString()), true, null, statusCode);
                } catch (JSONException e) {
                    Log.e("[STyler Error]", "Can not parse respond into JSON array");
                    callback.onRequestComplete(null, false, new STError("Can not parse respond into JSON array"), statusCode);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.w("[STyler Warning]", "resquest failed, respond: " + responseString);
                callback.onRequestComplete(null, false, new STError(responseString), statusCode);
            }
        };

        RequestParams params = new RequestParams();
        if (request.httpBody != null) {
            for (Map.Entry<String, Object> entry : request.httpBody.entrySet()
                    ) {
                params.put(entry.getKey(), entry.getValue());
            }
        }

        switch (request.httpMethod) {
            case GET:
                requestClient.get(request.url, params, respondhandler);
                break;
            case POST:
                requestClient.post(request.url, params, respondhandler);
                break;
            case PUT:
                requestClient.put(request.url, params, respondhandler);
                break;
            case PATCH:
                requestClient.patch(request.url, params, respondhandler);
                break;
            case DELETE:
                requestClient.delete(request.url, params, respondhandler);
                break;
            default:
                Log.e("[Styler Error]", "Unknown HTTP method: " + request.httpMethod.name());
                callback.onRequestComplete(null, false, null, -1);
                break;
        }
    }

    public static void sendGet(String path, Map<String, String> params, final STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError error, int statusCode) {
                if (error != null) {
                    completion.onRequestComplete(null, false, error);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null);
                } else {
                    if (json.isNull("message")) {
                        completion.onRequestComplete(json, true, null);
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(json.get("message").toString()));
                        } catch (Exception dunnoWthHappenHere) {}
                    }
                }
            }
        });
    }

    public static void sendGet(String path, Map<String, String> params, final STCompletion.WithJsonNullableInt completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, params);

        if (url == null) {
            completion.onRequestComplete(null, false, null, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError error, int statusCode) {
                if (error != null) {
                    completion.onRequestComplete(null, false, error, null);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null, null);
                } else {
                    if (json.isNull("message")) {
                        Integer lastObjID = null;
                        try {
                            lastObjID = json.getInt("last_object_id");
                        } catch(Exception e) {}
                        completion.onRequestComplete(json, true, null, lastObjID);
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(json.get("message").toString()), null);
                        } catch (Exception dunnoWthHappenHere) {}
                    }
                }
            }
        });
    }

    public static void sendPost(String path, Map<String, Object> httpBody, final  STCompletion.Base completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpBody = httpBody;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }
                completion.onRequestComplete(true, null);
            }
        });
    }

    public static void sendPost(String path, Map<String, Object> httpBody, final  STCompletion.WithJson completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(null, false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpBody = httpBody;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(null, false, err);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null);
                } else {
                    if (json.isNull("message")) {
                        try {
                            completion.onRequestComplete(json, true, null);
                        } catch (Exception parseJsonException) {
                            completion.onRequestComplete(null, false, new STError("No token received!"));
                        }
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(401, json.get("message").toString()));
                        } catch (Exception dunnoWthHappenHere) {}
                    }
                }
            }
        });
    }

    public static void sendPost(String path, Map<String, Object> httpBody, final  STCompletion.WithJsonToken completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(null, false, null, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpBody = httpBody;
        request.httpMethod = STEnums.HTTPRequestType.POST;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(null, false, err, null);
                    return;
                }

                if (json == null || json.length() <= 0) {
                    completion.onRequestComplete(null, false, null, null);
                } else {
                    if (json.isNull("message")) {
                        try {
                            completion.onRequestComplete(json, true, null, json.get("token").toString());
                        } catch (Exception parseJsonException) {
                            completion.onRequestComplete(null, false, new STError("No token received!"), null);
                        }
                    } else {
                        try {
                            completion.onRequestComplete(null, false, new STError(401, json.get("message").toString()), null);
                        } catch (Exception dunnoWthHappenHere) {}
                    }
                }
            }
        });
    }

    public static void sendPatch(String path, Map<String, String> param, final STCompletion.Base completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, param);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.PATCH;
        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }

                if(json != null)
                    completion.onRequestComplete(true, null);
                else
                    completion.onRequestComplete(false, null);
            }
        });
    }

    public static void sendDelete(String path, final STCompletion.Base completion) {
        String url = StylerAPIBaseRequests.urlWithPath(path, null);

        if (url == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        STURLRequest request = new STURLRequest(url);
        request.httpMethod = STEnums.HTTPRequestType.DELETE;
        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int status) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }

                if(json != null)
                    completion.onRequestComplete(true, null);
                else
                    completion.onRequestComplete(false, null);
            }
        });
    }
}

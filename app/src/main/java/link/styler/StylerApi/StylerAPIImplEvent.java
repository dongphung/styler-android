package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STStructs;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplEvent {
    public static void createEvent(STStructs.StylerCreateEventParam param, final STCompletion.WithJson completion) {

    }

    public static void patchEvent(STStructs.StylerPatchEventParam param, final STCompletion.Base completion) {

    }

    public static void getFeatureEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getOngoingEvents(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getEventDetail(int eventID, final STCompletion.WithJson completion) {

    }

    public static void deleteEvent(int eventID, final STCompletion.Base completion) {

    }

    public static void getEventsOfShop(int shopID, Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void getEventsOfUserWithID(int userID, Integer lastObjID, final STCompletion.WithJson completion) {

    }

    public static void createInterest(int eventID, final STCompletion.WithJson completion) {

    }

    public static void deleteInterest(int interestID, final STCompletion.Base completion) {

    }
}

package link.styler.StylerApi;

import android.util.Log;

import org.json.JSONObject;

import link.styler.CompletionInterfaces.STCompletion;
import link.styler.STStructs.STEnums;
import link.styler.STStructs.STError;
import link.styler.STStructs.STURLRequest;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplVersion {
    public static void getIsLatestVersion(String versionNumber, final STCompletion.Base completion) {
        String path = "/check_version?version_num=" + versionNumber;

        String urlString = StylerAPIBaseRequests.urlWithPath(path, null);
        if (urlString == null) {
            completion.onRequestComplete(false, null);
            return;
        }

        Log.i("[Styler]", "get version" + urlString);
        STURLRequest request = new STURLRequest(urlString);
        request.httpMethod = STEnums.HTTPRequestType.GET;

        StylerAPIBaseRequests.startRequest(request, new STCompletion.WithJsonInt() {
            @Override
            public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int statusCode) {
                if (err != null) {
                    completion.onRequestComplete(false, err);
                    return;
                }

                if (statusCode == 200)
                    completion.onRequestComplete(true, null);
                else
                    completion.onRequestComplete(false, null);
            }
        });
    }
}

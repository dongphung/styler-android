package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplAuth {
    public static void login(String email, String password, final STCompletion.WithJsonToken completion) {

    }

    public static void facebookLogin(String accessToken, final STCompletion.WithJsonToken completion) {

    }

    public static void logout(STCompletion.Base completion) {

    }

    public static void refreshToken(STCompletion.WithJsonToken completion) {

    }

    public static void reset(String email, final STCompletion.Base completion) {

    }
}

package link.styler.StylerApi;

import link.styler.CompletionInterfaces.STCompletion;

/**
 * Created by dongphung on 1/11/17.
 */

final class StylerAPIImplConversation {
    public static void getConversations(Integer lastObjID, final STCompletion.WithJsonNullableInt completion) {

    }

    public static void postConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
    }

    public static void deleteConversation(int conversationID, final STCompletion.Base completion) {
        // not use anywhere on iOS
    }
}

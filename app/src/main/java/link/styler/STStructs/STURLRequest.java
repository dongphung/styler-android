package link.styler.STStructs;

/**
 * Created by dongphung on 1/5/17.
 */

import java.net.URL;
import java.util.Map;

public class STURLRequest {
    public String url = "";
    public STEnums.HTTPRequestType httpMethod;
    public Map<String, Object> httpBody = null;

    // Do we need constructor or not?
    public STURLRequest() {

    }

    public STURLRequest(String _url) {
        url = _url;
    }
}

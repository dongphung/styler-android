package link.styler.STStructs;

/**
 * Created by dongphung on 1/12/17.
 */

public class STEnums {

    public enum FavoriteSortType {
        date,
        date_rev,
        shop,
        shop_rev;
    }

    public enum ReportType {
        Post,
        Reply,
        Comment,
        Item;
    }

    public enum HTTPRequestType {
        GET,
        POST,
        PUT,
        PATCH,
        DELETE;
    }
}

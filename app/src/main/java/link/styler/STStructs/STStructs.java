package link.styler.STStructs;

import android.graphics.Bitmap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dongphung on 1/12/17.
 */

public class STStructs {
    public class StylerReplyAndItemStruct {
        public int postID;
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public int categoryID;
        public int itemID;
    }

    public class StylerReplyStruct {
        public int postID;
        public String comment;
        public int itemID;
    }

    public class StylerUpdateItemAndReplyStruct {
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public int categoryID;
        public int itemID;
    }

    public class StylerUpdateItemStruct {
        public int replyID;
        public String comment;
        public String brand;
        public String name;
        public String price;
        public String text;
        public int categoryID;
    }

    public class Favorite {
        public String objectId;
        public String imageUrl;
    }

    public class StylerCreateEventParam {
        public Bitmap photoData;
        public String title;
        public String text;
        public String start_date;
        public String finish_date;
        public String location;

        public Map<String, Object> createBodyDataExceptPhotoDataFromSelf() {
            Map<String, Object> result = new HashMap<>();
            result.put("title", title);
            result.put("text", text);
            result.put("start_date", start_date);
            result.put("finish_date", finish_date);
            result.put("lcation", location);

            return result;
        }

        public Bitmap createPhotoDataFromSelf() {
            return photoData;
        }
    }

    public class StylerPatchEventParam {
        public int eventID;
        public Bitmap photoData;
        public String title;
        public String text;
        public String start_date;
        public String finish_date;
        public String location;

        public Map<String, Object> createBodyDataExceptPhotoDataFromSelf() {
            Map<String, Object> result = new HashMap<>();
            result.put("title", title);
            result.put("text", text);
            result.put("start_date", start_date);
            result.put("finish_date", finish_date);
            result.put("lcation", location);

            return result;
        }

        public Bitmap createPhotoDataFromSelf() {
            return photoData;
        }
    }
}

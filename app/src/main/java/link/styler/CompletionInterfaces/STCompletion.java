package link.styler.CompletionInterfaces;

import org.json.JSONObject;

import link.styler.STStructs.STError;

/**
 * Created by dongphung on 1/6/17.
 */

/* Interfaces for call back methods */
public interface STCompletion {
    public interface Base{
        public void onRequestComplete ( boolean isSuccess, STError err);
    }

    public interface Void{
        public void onRequestComplete ( );
    }

    public interface WithJson {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err);
    }

    public interface WithJsonInt {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, int intValue);
    }

    public interface WithJsonNullableInt {
        public void onRequestComplete(JSONObject json, boolean isSuccess, STError err, Integer nullableInt);
    }

    public interface WithJsonToken {
        public  void onRequestComplete(JSONObject json, boolean isSuccess, STError err, String token);
    }

    public interface WithNullableInt {
        public void onRequestComplete(boolean isSuccess, Integer nullableInt);
    }
}

/*
public abstract class STCompletionHandler {
    //public abstract void onRequestComplete(CompletionParams params);

    public abstract void onRequestComplete(JSONObject json, STError err, int statusCode);
    public abstract void onRequestComplete(JSONObject json, STError err);
}
*/
/* call back interface end */
